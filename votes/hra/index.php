<?php

$house = 7;
$newsid = 12;
$hcode = 'hra';

 require_once("../../web_config/globals19.php");
 header("content-type: text/html; charset=utf-8");
 $id = $help =  $class = $type = $controlBoxEnd = $controlBoxStart = $menu_table = $send = $menu = $nas_sou = $supercount = false;
 include "../..". Globals::$GLOBAL_SQL_FILE;
 @extract($_REQUEST);
 include_once("../../lib/lib.php"); 
 include_once("../../lib/settings.php");
 include_once("../../lib/priviledge_constants.php"); 
 include_once("../../lib/jury_user_functions.php"); 
 include_once("../../lib/security_functions.php");
 //include_once("./lib/log_functions.php");
  include_once("../../lib/mail_functions.php");
  include_once("../../lib/bootstrapForms.php");
  include_once("../../lib/pager.php");
  // require_once("./lib/juryLib.php");
 session_start();

 $_SESSION["file_info"] = array();
 if (!isset($_COOKIE["is_hidden"])) { $bodystyle = $logostyle = $sidebarstyle = ''; }else{ $bodystyle = "full"; $logostyle = 'class="fix"'; $sidebarstyle = 'class="active"'; }
 if (!isset($_COOKIE["is_3cols"])) { $formstyle  = ''; }else{ $formstyle = 'class="cols3"';   }
 
?>
<!DOCTYPE html>
<html lang="cs">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
<meta name="description" content="PANStuff | Facility Management"/>
<meta name="keywords" lang="cs" content=""/>
<meta name="resource-type" content="document"/>
<meta name="copyright" content="HandMade"/>
<meta name="author" content="HandMade"/>
<meta name="robots" content="no,no-FOLLOW"/>
<meta http-equiv="Content-language" content="cs"/>
<meta http-equiv="Cache-control" content="no-cache"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Expires" content="-1"/>
<title>PANStuff | Facility Management</title>

<link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
<link rel="manifest" href="/favicons/site.webmanifest">
<link rel="mask-icon" href="/favicons/safari-pinned-tab.svg" color="#d1d61e">
<meta name="msapplication-TileColor" content="#d1d61e">
<meta name="theme-color" content="#d1d61e">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" />
<script type="text/javascript" src="https://code.jquery.com/jquery-latest.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

<link rel="stylesheet" href="/css/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="/js/jquery-ui.js"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet"> 
<script type="text/javascript" src="./js/scripts.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="/js/superselect/chosen.jquery.js" type="text/javascript"></script>
<script src="/js/superselect/prism.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="/js/superselect/prism.css">
<link rel="stylesheet" href="/js/superselect/chosen.css">   
<link rel="stylesheet" href="/css/print.css" media='print'>   
<link rel="stylesheet" href="../../css/main.css" />
<link rel="stylesheet" href="<?php echo Globals::$GLOBAL_TEMP_DIR; ?>/css/response.css">   
	<?php
 
 function getUserIpAddr(){ 
	if(!empty($_SERVER['HTTP_CLIENT_IP'])){  $ip = $_SERVER['HTTP_CLIENT_IP']; }
		elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){ $ip = $_SERVER['HTTP_X_FORWARDED_FOR']; }
		else{ $ip = $_SERVER['REMOTE_ADDR']; 
		} return $ip; 
	} 

function passGen($long){

	$lt[0] = 'a';
	$lt[1] = 'b';
	$lt[2] = 'c';
	$lt[3] = 'd';
	$lt[4] = 'e';
	$lt[5] = 'f';
	$lt[6] = 'g';
	$lt[7] = 'h';
	$lt[8] = 'j';
	$lt[9] = 'k';
	$lt[10] = 'm';
	$lt[11] = 'n';
	$lt[12] = 'p';
	$lt[13] = 'r';
	$lt[14] = 's';
	$lt[15] = 't';
	$lt[16] = 'u';
	$lt[17] = 'v';
	$lt[18] = 'z';
	
	for ($k=0;$k<$long;$k++)
	   {
		$typ = rand(1, 2);
		if($typ == 1){
		  $ltpos =  rand(0, 18);
		  $pass .= $lt[$ltpos];
		}
		else{
		 $pass .= rand(0, 9);
		}
		
	
	   }
	   return $pass;
  }
	 


?>

</head>

<style>

select option[disabled] { color: #c0c0c0; font-style: italic }

	</style>

<body   class='<?php echo $bodystyle; ?> <?php echo $id; ?>'>
<div id='logo'><i class="fas fa-globe"></i><a href="./">PANSTUFF</a></div>
 
<div id="login">
 
</div>
 

<section id='<?php echo $_REQUEST["id"]; ?>' class='mysection'>
<?php


$query = "SELECT * FROM h".$house."meeting Where ID =  ".$newsid."  " ;
$result = $GLOBALS["link"]->query($query);

echo mysqli_error($GLOBALS["link"]);

if ($result && mysqli_num_rows($result) > 0) {
while ($row = mysqli_fetch_array($result)) {

 $meetingName = $row['meetingName'];
 $meetingDate = getDateFromSQL($row['meetingDate']);
 $meetingType = $row['meetingType'] ;
 $meetingNote = $row['meetingNote'];

}  
}




 
      if($_POST['sended'] == 1) {
	   
	   if($_POST['meetingID'] > 0) {
	   if(strlen($_POST['vote1']) > 0) { $myVote = 1; $hlas = 'PRO A'; }
	   if(strlen($_POST['vote2']) > 0) { $myVote = 2; $hlas = 'PRO B';  }
	   if(strlen($_POST['vote3']) > 0) { $myVote = 3; $hlas = 'PRO C';  }
	   $myCode = passGen(15);

		$upquery = "Update h".$house."meetingVotes Set meetingVote2Confirm  = ".$myVote."  Where ID = ".$_POST['meetingID']; 
		$result = $GLOBALS["link"]->query($upquery);
		
	    $upquery = "Update h".$house."meetingVotes Set meetingCode  = '".$myCode."'  Where ID = ".$_POST['meetingID']; 
		$result = $GLOBALS["link"]->query($upquery);

		$upquery = "Update h".$house."meetingVotes Set voteIP  = '".getUserIpAddr()."'  Where ID = ".$_POST['meetingID']; 
		$result = $GLOBALS["link"]->query($upquery);

		$mailQuery = "Select meetingEmail From h".$house."meetingVotes Where ID = ".$_POST['meetingID']; 
		$result = $GLOBALS["link"]->query($mailQuery);
		if ($result && mysqli_num_rows($result) > 0) {
			while ($rowEmail = mysqli_fetch_array($result)) {
				$to_email = $rowEmail['meetingEmail'];
			}
		}


	//	$to_email = 'zjk@honeypot.cz';
		$subject = 'Distancni hlasovani';
		$plaintext = 'Distancni hlasovani';
		$from_email = 'votes@panstav.cz';
		$from_name = 'Distancni hlasovani';
		$to_hello = 'Dobrý den';
		$mailtext = '<br/>Děkujeme. <br/><br/>';
	
	    
		$mailshortxt = "Vaším jménem bylo před chvílí odhlasováno v distančním hlasování <b>".$$meetingName."</b> Zaznamenaný hlas je: <br/><br/><b>".$hlas."</b>";
		$mailshortxt .= '<br/><br/>Pokud chcete hlasování potvrdit, klikněte na následující odkaz (případně ho zkopírujte a vložte do adresního řádku prohlížeče):<br/><br/>https://www.panstuff.cz/votes/'.$hcode.'/index.php?confirm='.$myCode;
		$mailshortxt .=  "<br/><br/>Děkujeme.";


	   
		$mailPlain = "<html>
		<head>
		  <title>Distanční hlasování</title>
		</head>
		<body>
		  <h3>Distanční hlasování</h3>
		  ".$mailshortxt."
		</body>
		</html>";
		
         

	   // $emailfile =  newsletter_create_file (1, $mailtext, $mailshortxt, $mailhallo, $client);
		//sendEmailPrepared($emailfile,$to_email,$subject,$plaintext,$from_email,$from_name,$to_hello,$attfile,$client,$clientlogo, $superfile) ;
		// sendEmailNEW2018($subject,$mailshortxt,$to_email,$from_email,$from_name);  

		//  sendEmailNEW($subject,$mailshortxt,$to_email,$from_emai,$from_name);
	//	mail()

 

		$headers[] =  $to_email ;
		echo $headers[] = 'From: '.$from_name.' <'.$from_email.'>';
		$headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type: text/html; charset=utf-8';
	 
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= 'From: <'.$from_email.'>' . "\r\n";
 

	 	mail ( $to_email , $subject , $mailPlain , $headers ) ;

		$infotext = "Na e-mail, který máme zaznamenaný u vašeho vlastnického podílu, jsme odeslali žádost o potvrzení hlasování. <br/> Pokud vám žádost nepřijde, nebo pokud nevíte, o jakou e-mailovou adresu se jedná, kontaktujte správce. ";
		$alertype = " alert-success ";

	  }else{

		$infotext = "Musíte vybrat, za jakou jednotku hlasujete. ";
		$alertype = " alert-danger ";

	  }
	  }
 
 
 ?>
 
 
 <h1>Distanční hlasování - <?php echo $meetingName ; ?> | do <?php echo $meetingDate ; ?>  </h1> 

<p>

<?php  




if(isset($_GET['confirm'])) {
 
    
 	$confirmQuery  = "Select * From h".$house."meetingVotes Where meetingCode = '".$_GET['confirm']."'"; 
	$resultC = $GLOBALS["link"]->query($confirmQuery);
	if ($resultC && mysqli_num_rows($resultC) == 1) {
		while ($rowC = mysqli_fetch_array($resultC)) {
			
			$myID = $rowC['ID'];
			$myVote = $rowC['meetingVote2Confirm'];
	        $upquery = "Update h".$house."meetingVotes Set meetingVote  = '".$myVote."'  Where ID = ".$myID; 
			$result = $GLOBALS["link"]->query($upquery);
	        $upquery = "Update h".$house."meetingVotes Set meetingCode  = '".$myVote."'  Where ID = ".$myID; 
			$result = $GLOBALS["link"]->query($upquery);	
			$upquery = "Update h".$house."meetingVotes Set confirmIP  = '".getUserIpAddr()."'  Where ID = ".$myID; 
			$result = $GLOBALS["link"]->query($upquery);
			
			

			$infotext = "Hlasování je potvrzeno. ";
			$alertype = " alert-success ";

		}
	}
	else {

		$infotext = "Nezámý kód potvrzení. ";
		$alertype = " alert-danger ";

	}


	
	}
else{


echo $meetingNote;


}
if (@$infotext != "") {
    ?>
        <div class="alert <?php echo $alertype; ?>" role="alert"><?php echo $infotext; ?></div>
        <?php
}


?>




</p>

<div class="alert alert-secondary" style="font-size: 80%" role="alert">
Vyberte, za jaký vlastnický podíl hlasujete a odhlasujte. Na vybraný e-mail vám přijde žádost o potvrzení volby.
Jednotky, které neodhlasují, nebo hlasování nepotvrdí, budou evidovány, jako nepřítomné.
</div>

<form ENCTYPE="multipart/form-data" action="#" method="post">

<div class="input-group mb-3">
			<div class="input-group-prepend">
			<span class="input-group-text" id="inputGroup-sizing-default">Kdo jste?</span>
			</div>
			<select  class="form-control  chosen-select  " id="exampleFormControlSelect2" name="meetingID">
            <option value='-1' >Vyberte jednotku</option>
 <?php

echo $query = "SELECT *,ID as THISID FROM h".$house."meetingVotes Where meetingID =  ".$newsid." Order by meetingFlat  " ;
$result = $GLOBALS["link"]->query($query);
$flatSquare = $totalDeal = 0;
if ($result && mysqli_num_rows($result) > 0) {
    while ($row = mysqli_fetch_array($result)) {

		 if(strlen(trim($row['meetingEmail'])) < 2) { $disme = ' disabled '; $distxt = ' | Chybí E-mail'; }  
		 elseif($row['meetingVote'] > 0) { $disme = ' disabled '; $distxt = ' | Odhlasováno'; } 
		 elseif($row['meetingPresent'] == 0) { $disme = ' disabled '; $distxt = ' | Blok'; } 
		 else { $disme = ''; $distxt = '';  }

		
        ?>
		<option <?php echo $disme ; ?> value='<?php echo $row['ID']; ?>' ><?php echo "Byt č. ".$row['meetingFlat']."  ".  $row['meetingPerson']    ; ?> <?php echo $distxt ; ?> </option> 
		<?php


	}
}
?>


          


    	</select>



	</div>
      <input type='hidden' value='1' name='sended' />


	  <input type='submit' value='Hlasuji PRO A' class='ano btn btn-success' name='vote1'/>
	  <input type='submit' value='Hlasuji PRO B' class='ne btn btn-danger' name='vote2'/>
	  <input type='submit' value='Hlasuji PRO C' class='nwm btn btn-secondary' name='vote3' />
    </form>

</section>
<footer>
 
 
<a href='http://www.honeypot.cz' id='honeypot' target="_blank" ><span>Created by</span>HONEYPOT</a>
</footer>
 
</body>
</html>  

<?php

$included_files = get_included_files();

foreach ($included_files as $filename) {
   // echo "$filename\n";
}

?>