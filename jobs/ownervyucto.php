<?php
 //header("content-type: text/html; charset=utf-8");

$today = date("Y-m-d");
$owner =  $_GET['owner'] ; 
$from =  $_GET['from'] ; 
$to =  $_GET['to'] ; 
$house =  $_GET['house'] ; 
$year =  $_GET['year'] ; 
$table_name = "h".$_GET['house']."charges";

 require_once("../web_config/globals19.php");
 require_once("../lib/lib.php");
include "..". Globals::$GLOBAL_SQL_FILE;

 error_reporting(E_ERRORS);
 ini_set('display_errors', 1);




  $style2  = 
  "<style>    
  @page Section2 {size:861.7pt 595.45pt;mso-page-orientation:landscape;margin:1in 1.0in 1in 1.0in;mso-header-margin:.3in;mso-footer-margin:.3in;mso-paper-source:0;}
  div.Section2 {page:Section2;}
  body { font-family: Arial, sans-serif; font-size: 10px;}
  p {  font-family:  Arial, sans-serif; font-size: 11px; line-height: 1.1em }
  table.lines, table.lines td, table.lines th { border: 1px solid #c0c0c0; border-collapse: collapse; text-align: left; }
  table.lines td, table.lines th { padding: 5px 10px; }
  table { width: 100%; font-size: 10px}
  .size2 { font-size: 13px; }
  .size3 { font-size: 19px; }  
  .number { text-align: right!important; } 
  .center { text-align: center!important; }
  .bold {  font-weight: bold; }
  .expl {  font-style: italic;  }
  td {  vertical-align: top; }
  .nolines td {  padding-right: 1em; }
  .right { text-align: right; }
  .preplatek, .nedoplatek {
     display: inline-block;
     padding-top:  0em;
     margin-top: 0.5em;
     border: 0px solid #e7e7e7;
      
     clear: right;
     font-family: sans-serif; font-size: 10px; 
  }
   span.bgr { font-size: 200% ; line-height: 0.5em}
  .preplatek { font-size: 20px; color: #3de51b; font-weight: bold; font-family: Arial, sans-serif;   }
  .nedoplatek { font-size: 20px; color: red; font-weight: bold; font-family:Arial, sans-serif;   }
 </style>";

    if($owner > 0) {
     $bejvaky = "SELECT * FROM `h".$house."persons`,h".$house."flats  WHERE h".$house."flats.ID = flatID And `personID` = ".$owner." AND ( `personOwner` = 4 OR `personOwner` = 2 ) Group by flatID Order by flatOrder";
      $names = "Byty vlastníka: ".getPersonByID($owner);
    }
    else{
      $bejvaky = "SELECT * FROM `h".$house."persons`,h".$house."flats Where h".$house."flats.ID = flatID   Group by flatID  Order by flatOrder"; 

 
    }


    $txt .= "<div class=Section2>

            
    <table class='nolines' border='0'>
    <tr>
    <td class='size2 perc70'>Přehled vyúčtování za období od ".$from.".".$year." do ".$to.".".$year."  </td>
    <td class='number'>Generováno:</td>
    <td class='bold'>".date("d. m. Y",time())."</td>
    </tr>
    <tr>
    <td class='size3 perc70' rowspan='3'>Objekt: ".getHouseNickByID($house)."<br/>".$names."</td>
    <td class='number'> </td>  
    <td class='bold'> </td>
    </tr>
    <tr>
    <td class='number'  > </td>
    <td class='bold'> </td>
    </tr>
    <tr>
    <td class='number'  > </td>
    <td class='bold'> </td>
    </tr>
    </table>
   ";

   $txt  .= "<br/><table class='lines' >";
   $txt  .= "<tr style='background:#efefef'>
   <th> Byt </th>
   <th class='center noFlat'>Předepsáno</th>
   <th class='center' colspan=3 >Náklady</th>
   <th class='center'>Rozdíl</th>
   <th class='center'>Platby</th>
   <th class='center'>Vyúčtování</th>
   </tr>";

   $txt  .= "<tr>
   <th> Číslo jednotky </th>
   <th class='center noFlat'>Součet za období</th>
   <th class='center noFlat'>Služby - reálné náklady</th>
   <th class='center'>Fond oprav (nájem)</th>
   <th class='center'>Náklady celkem</th>
   <th class='center'>Předpisy vs. náklady </th>
   <th class='center'>Evidované platby</th>
   <th class='center'>Platby - náklady</th>
   </tr>";
   
    $where2 = " And chargesDate  >= '".$year."-".$from."-01'  And  chargesDate  <=  '".$year."-".$to."-31'    "    ;  

   $resbejvaky = $GLOBALS["link"]->query($bejvaky);
   if ($resbejvaky && mysqli_num_rows($resbejvaky) > 0) {
       while ($rowbejvaky = mysqli_fetch_array($resbejvaky)) { 

        $txt  .= "<tr>";

        $totalIN =  $totalFO = $totalOUT = $total = 0;

        $txt  .= "<td>".getFlatNrFromID($rowbejvaky['flatID'],$house)."</td>";

         /*  Query na předpisy */
         $query = "SELECT chargesAmount as TOTAL FROM h".$_GET['house']."flats, h".$_GET['house']."persons , ".$table_name." 
         LEFT JOIN fm_services ON fm_services.ID = chargesService 
         Where   h".$_GET['house']."persons.flatID =  h".$_GET['house']."flats.ID  And    h".$_GET['house']."flats.ID = ".$table_name.".chargesFlat And ( chargesAmount > 0  )  And chargesFlat = ".$rowbejvaky['flatID']. $where2 ."   Group by h".$_GET['house']."charges.ID Order by chargesDate DESC, chargesTime DESC  ";
      
        $result = $GLOBALS["link"]->query($query);
        if ($result && mysqli_num_rows($result) > 0) {
          while ($row = mysqli_fetch_array($result)) { 

           
            $total = $total +    $row['TOTAL'];

          }
        }
        $superTotal = $superTotal + $total;
        $txt  .= "<td class='number'>". numberfix($total)."</td>";
        
      
       /*  Query na náklady  */
      
       $query = "SELECT chargesOUT as totalOUT FROM h".$_GET['house']."flats, h".$_GET['house']."persons , ".$table_name." 
       LEFT JOIN fm_services ON fm_services.ID = chargesService 
       Where   h".$_GET['house']."persons.flatID =  h".$_GET['house']."flats.ID  And    h".$_GET['house']."flats.ID = ".$table_name.".chargesFlat And ( chargesOUT > 0  )  And chargesFlat = ".$rowbejvaky['flatID']. $where2 ."   Group by h".$_GET['house']."charges.ID Order by chargesDate DESC, chargesTime DESC  ";
    
      $result = $GLOBALS["link"]->query($query);
      if ($result && mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_array($result)) { 

         
          $totalOUT = $totalOUT +    $row['totalOUT'];

        }
      }
      $superTotalOUT = $superTotalOUT + $totalOUT;
      $txt  .= "<td class='number'>". numberfix($totalOUT)."</td>";
           

      /*  Query na FO  */
      
        $query = "SELECT chargesAmount as totalFO FROM h".$_GET['house']."flats, h".$_GET['house']."persons , ".$table_name." 
        LEFT JOIN fm_services ON fm_services.ID = chargesService 
        Where   h".$_GET['house']."persons.flatID =  h".$_GET['house']."flats.ID  And    h".$_GET['house']."flats.ID = ".$table_name.".chargesFlat And ((   chargesService = 9 ) OR ( chargesService = 8 )  OR ( chargesService = 29 ) )  And chargesFlat = ".$rowbejvaky['flatID']. $where2 ."   Group by h".$_GET['house']."charges.ID Order by chargesDate DESC, chargesTime DESC  ";

        $result = $GLOBALS["link"]->query($query);
        if ($result && mysqli_num_rows($result) > 0) {
          while ($row = mysqli_fetch_array($result)) { 

   
          $totalFO = $totalFO + $row['totalFO'];

          }
          } 
          $superTotalFO = $superTotalFO + $totalFO;
          $txt  .= "<td class='number'>". numberfix($totalFO)."</td>";


          $balance = $total - $totalFO - $totalOUT;
          $grandTotal = $totalOUT + $totalFO;
          $superTotalGRAND = $superTotalGRAND + $grandTotal;
          $txt  .= "<td class='number'>". numberfix($grandTotal)."</td>";
          $superBalance = $superBalance + $balance;
          $txt  .= "<td class='number'>". numberfix($balance)."</td>";

      /*  Query na Platby  */
      
      $query = "SELECT chargesIN as totalIN FROM h".$_GET['house']."flats, h".$_GET['house']."persons , ".$table_name." 
      LEFT JOIN fm_services ON fm_services.ID = chargesService 
      Where   h".$_GET['house']."persons.flatID =  h".$_GET['house']."flats.ID  And    h".$_GET['house']."flats.ID = ".$table_name.".chargesFlat And (  chargesIN > 9 )  And chargesFlat = ".$rowbejvaky['flatID']. $where2 ."   Group by h".$_GET['house']."charges.ID Order by chargesDate DESC, chargesTime DESC  ";

      $result = $GLOBALS["link"]->query($query);
      if ($result && mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_array($result)) { 

         
        $totalIN = $totalIN + $row['totalIN'];

        }
        } 
        $superTotalIN = $superTotalIN + $totalIN;
        $txt  .= "<td class='number'>". numberfix($totalIN)."</td>";

        $totalbalance = $totalIN - $totalFO - $totalOUT;

        $superTotalBalance = $superTotalBalance + $totalbalance;
        $txt  .= "<td class='number'>". numberfix($totalbalance)."</td>";


        $txt  .= "</tr>";
      }      
   }
   

   $txt  .= "<tr style='background:#efefef'>
   <th> CELKEM </th>
   <th class='number'>".numberFix($superTotal)."</th>
   <th class='number'>".numberFix($superTotalOUT)."</th>
   <th class='number'>".numberFix($superTotalFO)."</th>
   <th class='number'>".numberFix($superTotalGRAND)."</th>
   <th class='number'>".numberFix($superBalance)."</th>
   <th class='number'>".numberFix($superTotalIN)."</th>
   <th class='number'>".numberFix($superTotalBalance)."</th>
   </tr>";


   $txt .= "</table>";
   

         
 

  $txt .= "<p class='right'>Vaše konto vykazuje za období od ".$from.".".$year." do ".$to.".".$year." ";
  $txt .= "</p><p  class='right'>";

  $balance =  $superTotalBalance;
  

  if($balance > 1) {  
    $preFIX = "P-";
    $txt .= "<strong class='preplatek'>PŘEPLATEK:  ".numberfix($balance)."</strong>
    <br/><br/><span>
 
    Pokud platíte zálohy převodem z účtu, bude vám přeplatek poukázán na tento účet. <br/>Pokud platíte jiným způsobem, kontaktujte nás na 0602 589 238</span>
    ";
  }
  elseif($balance < 0)  {
    $preFIX = "N-";
    $txt .= "<strong class='nedoplatek'>NEDOPLATEK: ".numberfix(($balance)*-1)."</strong>
    <br/><br/><span>
  
    Prosíme uhraďte nedoplatek obvyklým způsobem (bankovní účet domu: <b>".getHouseBankByID($house)."</b>) do tří měsíců od vyúčtování. <br/>Prosíme, uveďte do poznámky, že jde o doplatek za rok <b>2019</b>. <br/>Děkujeme.</span>
    ";
  }
  else{
    $preFIX = "OK-";
    $txt .= "<strong class='preplatek'>VYROVNANÝ STAV:  ".numberfix($balance)."</strong>
    <br/> <span>
 
     
    ";
  }
 
   $txt .= "
   
 
   
   
   
   </p>";
   $txt .= "<span style='position:absolute; left: 0; color: #c0c0c0; cursor: pointer;' id='copyButton'>file: ".$_GET['load']." <br/>
   <input style='color: #fff; font-size: 90%; border: 0' type='text; cursor: pointer;' id='copyTarget' value='".makeLinkFromUTF($fnames.$fbrk.$preFIX.$fnames1.$fnames2)."'/></span>";
   $txt .= "<p style='text-align:right'>
   <img  src='https://www.panstuff.cz/jobs/zjkSignatureSMALL.png' />
   </p>";
 
 
$html = '<html>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
<body  >
';
//$txt = str_replace  ( "\n" , '<br/>'  , $txt);
$html .= $style2;
$html .= $txt ;
/* 
$html .='
<script>
document.getElementById("copyButton").addEventListener("click", function() {
  copyToClipboard(document.getElementById("copyTarget"));
  window.print();
});

function copyToClipboard(elem) {
  var targetId = "_hiddenCopyText_";
  var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
  var origSelectionStart, origSelectionEnd;
  if (isInput) {
      // can just use the original source element for the selection and copy
      target = elem;
      origSelectionStart = elem.selectionStart;
      origSelectionEnd = elem.selectionEnd;
  } else {
      // must use a temporary form element for the selection and copy
      target = document.getElementById(targetId);
      if (!target) {
          var target = document.createElement("textarea");
          target.style.position = "absolute";
          target.style.left = "-9999px";
          target.style.top = "0";
          target.id = targetId;
          document.body.appendChild(target);
      }
      target.textContent = elem.textContent;
  }
  // select the content
  var currentFocus = document.activeElement;
  target.focus();
  target.setSelectionRange(0, target.value.length);
  
  // copy the selection
  var succeed;
  try {
      succeed = document.execCommand("copy");
  } catch(e) {
      succeed = false;
  }
  // restore original focus
  if (currentFocus && typeof currentFocus.focus === "function") {
      currentFocus.focus();
  }
  
  if (isInput) {
      // restore prior selection
      elem.setSelectionRange(origSelectionStart, origSelectionEnd);
  } else {
      // clear temporary content
      target.textContent = "";
  }
  return succeed;
}
</script>

 */


$html .= '</body> </html>';

 
 





echo $html;
?>
