<?php
header("content-type: text/html; charset=utf-8");
require_once '../web_dompdf/autoload.inc.php';
 // reference the Dompdf namespace
use Dompdf\Dompdf;
// instantiate and use the dompdf class
$dompdf = new Dompdf();

$today = date("Y-m-d");
$myid =  $_GET['nr'] ; 

 
 require_once("../web_config/globals19.php");
 require_once("../lib/lib.php");
include "..". Globals::$GLOBAL_SQL_FILE;

 error_reporting(E_ERRORS);
 ini_set('display_errors', 1);




  $style2  = 
  "<style>    
  @page Section2 {size:861.7pt 595.45pt;mso-page-orientation:landscape;margin:1in 1.0in 1in 1.0in;mso-header-margin:.3in;mso-footer-margin:.3in;mso-paper-source:0;}
  div.Section2 {page:Section2;}
  body { font-family: Arial, sans-serif; font-size: 10px;}
  p {  font-family:  Arial, sans-serif; font-size: 10px; }
  table.lines, table.lines td, table.lines th { border: 1px solid #c0c0c0; border-collapse: collapse; text-align: left; }
  table.lines td, table.lines th { padding: 5px 10px; }
  table { width: 100%; font-size: 10px}
  .size2 { font-size: 13px; }
  .size3 { font-size: 19px; }  
  .number { text-align: right!important; } 
  .center { text-align: center!important; }
  .bold {  font-weight: bold; }
  td {  vertical-align: top; }
  .nolines td {  padding-right: 1em; }
  .right { text-align: right; }
  .preplatek, .nedoplatek {
     display: inline-block;
     padding-top:  1em;
     margin-top: 1em;
     border: 0px solid #e7e7e7;
      
     clear: right;
     font-family: sans-serif; font-size: 10px;
    
  }
  .preplatek { font-size: 20px; color: #3de51b; font-weight: bold; font-family: Arial, sans-serif;   }
  .nedoplatek { font-size: 20px; color: red; font-weight: bold; font-family:Arial, sans-serif;   }
 </style>";

    $fixDOC = "Update prntSanon Set sanonLoaded = 1 Where ID = ".$_GET['load']; 
    $resultFIX = $GLOBALS["link"]->query($fixDOC);

    $del = "DELETE FROM `prntSanon` WHERE `sanonLoaded` = 0";
    $delRes = $GLOBALS["link"]->query($del);

   $loadDOC = "  SELECT * FROM prntSanon  Where ID = ".$_GET['load'];
    $resultDOC = $GLOBALS["link"]->query($loadDOC);
    if ($resultDOC && mysqli_num_rows($resultDOC) > 0) {
    while ($rowDOC = mysqli_fetch_array($resultDOC)) { 
      
      if($rowDOC['sanonFlat']   >0) { $fnames = $names  = "Jednotka č. ".getFlatNrFromID($rowDOC['sanonFlat'],$rowDOC['sanonHouse']);  $brk='<br/>'; $fbrk = '-'; }
      if($rowDOC['sanonOwner']  >0) { $names1 = $brk."Byt/y vlastníka: ". getPersonByID($rowDOC['sanonOwner']);  $fnames1 =  getPersonByID($rowDOC['sanonOwner']); }
      if($rowDOC['sanonTenant'] >0) { $names2 = $brk."Uživatel bytu/: ". getPersonByID($rowDOC['sanonTenant']);  $fnames2 =  getPersonByID($rowDOC['sanonTenant']); }

      $houseQ = "Select * From fm_house Where ID = ".$rowDOC['sanonHouse'];
      $resultH = $GLOBALS["link"]->query($houseQ);
      if ($resultH && mysqli_num_rows($resultH) > 0) {
          while ($rowH = mysqli_fetch_array($resultH)) { 

            $txt .= "<div class=Section2>
            <table class='nolines' border='0'>
            <tr>
            <td class='size2 perc70'>Přehled vyúčtování za období od 1.1.".$rowDOC['sanonYear']." do 31.12.".$rowDOC['sanonYear']."  ".$title."</td>
            <td class='number'>Generováno:</td>
            <td class='bold'>".date("d. m. Y",time())."</td>
            </tr>
            <tr>
            <td class='size3 perc70' rowspan='3'>Objekt: ".$rowH['fm_nickname']."<br/>".$names.$names1.$names2."</td>
            <td class='number'>Var. symbol:</td>  
            <td class='bold'>".getFlatVSFromID($rowDOC['sanonFlat'],$rowDOC['sanonHouse'])."</td>
            </tr>
            <tr>
            <td class='number'  >Kontakt:</td>
            <td class='bold'>".getPhoneByID($rowDOC['sanonOwner'])." | ".getPhoneByID($rowDOC['sanonTenant'])."</td>
            </tr>
            <tr>
            <td class='number'  >E-mail:</td>
            <td class='bold'>".getEmailByID($rowDOC['sanonOwner'])." | ".getEmailByID($rowDOC['sanonTenant'])."</td>
            </tr>
            </table>
            <p>&nbsp;</p>";

          }
        }




       // header
        $txt  .= "<table class='lines' >";
        $txt  .= "<tr>
        <th> </th>
        <th colspan='3' class='center'>Množství</th>
        
        <th colspan='3'  class='center'>Spotřeba</th>
        
        <th colspan='2'  class='center'>Náklady</th>
        <th></th>
        <th></th>
     
        
        </tr>
        <tr>
        <th > </th>
        <th class='number'>Na objekt</th>
        <th class='number'>Celkem</th>
        <th>Způsob rozúčtování</th>
        <th class='number'>Na objekt</th>
        <th class='number'>Celková</th>
        <th>MJ</th>
        <th class='number'>Na objekt</th>
        <th class='number'>Celkové</th>

        <th class='number'> Zaplaceno</th>
        <th class='number'>Rozdíl</th>
        </tr>";

//fo
 
        $txt .= "<tr style='background:#efefef'>
        <td>FO/Nájem</td>
        <td class='number'>".numberfix($rowDOC['sanonDeal'] )."</td>
        <td class='number'>100</td>
        <td>Podíl bytové jednotky</td>
        <td class='number'>".numberfix($rowDOC['sanonDeal']/100 )."</td>
        <td class='number'>1</td>
        <td></td>
        <td class='number'>".numberfix($rowDOC['sanonMoney1'] )."</td>
        <td class='number'>".numberfix($rowDOC['money1TotalFO'] )."</td>
        <td class='number'>".numberfix($rowDOC['money1PaidFO'] )."</td>";
         
        $balFO = $rowDOC['money1PaidFO'] - $rowDOC['sanonMoney1'];
         
        $txt .= "<td class='number'>".numberfix($balFO )."</td>
        </tr>";

        $nasluzbyRest = $rowDOC['sanonPayments'] - $rowDOC['money1PaidFO'];
        $totalRozdil = $balFO;

        //services

      $services = "SELECT * FROM  prntServices,fm_services  Where prntServices.prntSErviceID = fm_services.ID And  sanonID = ".$rowDOC['sanonTimeID'];
        $resultS = $GLOBALS["link"]->query($services);
        if ($resultS && mysqli_num_rows($resultS) > 0) {
             while ($rowS = mysqli_fetch_array($resultS)) { 
//prntServiceStyle,serviceName,prntServiceMyDeal ,prntServiceUnitsTotal,prntServiceUnit,prntServiceMyCharge,prntServiceTotal
              if($rowS['prntServiceStyle'] == 10) {  $style = "Osoby";  }
              elseif($rowS['prntServiceStyle'] == 20) {  $style = "Měřidlo, výčet";  }
              elseif($rowS['prntServiceStyle'] == 30) {  $style = "Rovný díl";  }
              else{  $style = "";  }

               
               

              $txt .= "<tr>    
              <td> ".$rowS['serviceName']."</td>
              <td class='number'> ";
              if($rowS['prntServiceID'] == 6){
                 
                if($rowS['prntServiceMyDeal']>0){
                  $txt .= numberfix(1);
                }else{
                  $txt .= numberfix(0);
                }
               

              }else{

                if($rowS['prntServiceStyle'] == 20) { 
                  $txt .= numberfix($rowS['prntServiceMyDeal'] );
                }
                elseif($rowS['prntServiceStyle'] == 10)   {
                  $txt .= numberfix($rowDOC['sanonMPersons']);
                }
                else {
                
                }

              }
            
              //Mnozstvi Celkem
              $txt .= "</td><td class='number'>   ";

              if($rowS['prntServiceStyle'] == 10) { $gtxt  = $rowDOC['sanonHousePersons'] ;  }	
              if($rowS['prntServiceID'] == 24) { $gtxt  = $rowDOC['sanonLiftPersons'] ;  }	
              if($rowS['prntServiceStyle'] == 20) { if($rowS['prntServiceUnitsTotal'] > 0)  {  $gtxt  =   $rowS['prntServiceUnitsTotal'] ;  }  }
              if($rowS['prntServiceID'] == 6){   $gtxt  = $rowS['prntServiceUnitsTotal'] ;   } 

              if($rowS['prntServiceUnitsTotal'] > 0)  { 
               // $gtxt  =  numberfix($rowS['prntServiceUnitsTotal'] );
              }

              $txt .= $gtxt;

              //Spotreba 
              $txt .= " </td><td> ".$style."</td><td class='number'> ";
              
            
              if($rowS['prntServiceStyle'] != 10)
              {
                $txt .= numberfix($rowS['prntServiceMyDeal'] );
              }
              elseif($rowS['prntServiceID'] == 6) {
                   $txt .= $rowS['prntServiceUnitsTotal'] ; 
              }
              else{
                if($rowS['prntServiceUnitsTotal'] > 0){
                $txt .= numberfix(    $rowS['prntServiceMyCharge'] /   (  $rowS['prntServiceTotal']/$rowS['prntServiceUnitsTotal']     )   );
                }
                else{
                  $txt .= numberfix(   $rowDOC['sanonMPersons'] / $rowDOC['sanonHousePersons']  );
                }
              }
              $txt .= " </td>
              <td class='number'> "; 
              
              if($rowS['prntServiceUnitsTotal'] > 0)  { 
                  $txt .=   $rowS['prntServiceUnitsTotal']  ;
                  }
                  else{
                    $txt .= "1";
                  }
              $txt .= "</td>
              <td class=''> ".$rowS['prntServiceUnit']."</td>
              <td class='number'> ".numberfix($rowS['prntServiceMyCharge'] )."</td>
              <td class='number'> ".numberfix($rowS['prntServiceTotal'] )."</td>";
         
              if(($nasluzbyRest -  $rowS['prntServiceMyCharge']) >= 0) {

                    $serPAy =  $rowS['prntServiceMyCharge'];
                    $nasluzbyRest = $nasluzbyRest - $serPAy ;


              }
              else {

                     $serPAy = $nasluzbyRest ;
                     $nasluzbyRest = 0;

              }
              $rozdil = $serPAy - $rowS['prntServiceMyCharge']   ;
              $totalRozdil = $totalRozdil + $rozdil ;

              $txt .= "<td class='number'> ".numberfix($serPAy)."  </td>
              <td class='number'>".numberfix($rozdil)."</td>
              </tr>";
              $totalZalohy = $totalZalohy + $rowS['prntServiceMyCharge'];
              $totalZalohyHouse = $totalZalohyHouse + $rowS['prntServiceTotal'];


            }
          }
 




//zalohy

$nasluzby = $rowDOC['sanonPayments'] - $rowDOC['money1PaidFO'];

$total1 = $rowDOC['sanonMoney1'] + $totalZalohy;
$total2 = $rowDOC['money1PaidFO'] + $nasluzby;

$txt .= "<tr  style='background:#efefef'>
<td>CELKEM za jednotku</td>
<td class='number'></td>
<td class='number'> </td>
<td></td>
 
<td class='number'></td>
<td class='number'></td>
<td></td>
<td class='number bold'>".numberfix( $total1)."</td>
<td class='number'> </td>
 
<td class='number bold'>".numberfix( $total2)."</td>";
 
$balSE =  $nasluzby - $totalZalohy;
 
$txt .= "<td class='number bold'>".numberfix($balSE)."</td>
</tr>";

    }
  }

 
  $txt .= " </table>   ";
  
  $txt .= "<p class='right'>Vaše konto vykazuje za období od 1.1.".$rowDOC['sanonYear']." do 31.12.".$rowDOC['sanonYear']." ";
  $txt .= "</p><p  class='right'>";

  $balance = $balFO+$balSE;
  if($balance > 1) {  
    $txt .= "<strong class='preplatek'>PŘEPLATEK:  ".numberfix($balance)."</strong>
    <br/><br/><span>Pokud platíte zálohy převodem z účtu, bude vám přeplatek poukázán na tento účet. <br/>Pokud platíte jiným způsobem, kontaktujte nás na 0602 589 238</span>
    ";
  }
  elseif($balance < 0)  {
    $txt .= "<strong class='nedoplatek'>NEDOPLATEK: ".numberfix(($balance)*-1)."</strong>
    <br/><br/><span>Prosíme uhradite nedoplatek obvyklým způsobem do tří měsíců od vyúčtování. <br/>Děkujeme.</span>
    ";
  }
  else{
    $txt .= "<strong class='STAV '>0</strong>";
  }
   $txt .= "
   
 
   
   
   
   </p>";

   $txt .= "<p style='text-align:right'><img  src='".Globals::$GLOBAL_HOME."jobs/zjkSignature.png' /></p>";
 
 
$html = '<html>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
<body  >
';
//$txt = str_replace  ( "\n" , '<br/>'  , $txt);
$html .= $style2;
$html .= $txt ;
$html .='
</body>
</html>
';

$dompdf->loadHtml( $html );
// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'landscape');

 
// Render the HTML as PDF
$dompdf->render();
// Output the generated PDF to Browser
//$dompdf->stream();
 
 

$dompdf->stream("Vyuctovani-".makeLinkFromUTF($fnames.$fbrk.$fnames1.$fnames2).".pdf");
 
 
?>