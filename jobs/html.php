<?php

$style2 = "
  table { width: 100%;  font-size: 10px;   }
  img { float: left; margin-bottom: 10px }
  table.line, table.singleline {  border-collapse: collapse;   }
  table.line td, table.line th { border: 1px solid #e7e7e7;   text-align: left; }
  table.line th {  vertical-align: middle }
  table.line  td.right, table.line  th.right  { text-align: right; }
  table.line  th.total { background: #e7e7e7!important;   } 
  table.singleline td { border: 1px solid #e7e7e7; text-align: left; }
  table.singleline th { border: 1px solid #000000; text-align: left; }
  table.singleline td, table.singleline th { border-top: 1px solid #fff; border-left: 1px solid #fff; border-right: 1px solid #fff; }
  table.line td, table.line th { padding: 3px 10px; }
  table.singleline td, table.singleline th { padding: 3px 0px; }
</style>";
 
$txt1  = "
<table  >
<tr>
<td><h1>Evidenční list</h1></td>
<td style='width: 100px;'>".$logo."</td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
</table>

<table class='line' >
    <tr >
        <td colspan='2'><strong>Příjmení a jméno nájemce:</strong> </td>
        <td colspan='2'><h3>".$row['personSurname']." ".$row['personName']."</h3></td>
      </tr>
     <tr>

        <td>Číslo jednotky:</td><td> <strong>".$row['flatNr']."</strong> </td>
        <td>Patro: </td><td>".$row['flatLevel']." </td>
    </tr>
    <tr>
    <td> Bank. účet: </td>
    <td> <strong>".$row['fm_owner_bankNr']."</strong> (".$row['fm_owner_bank'].") </td>
    <td>Splatnost předpisu: </td><td>".$row['flatPayDate']."  </td>
        
         
    </tr>
    <tr>
    <td>Variabilní symbol: </td><td><strong>".$row['flatVS']."</strong>  </td>
    <td  >Druh: </td><td style='width: 16%'>".$row['flatTypeFlat']."</td> 
    
    </tr>
  <tr>
  <td>Tisk dne: </td><td>".date('d.m.Y')." </td> 
  <td>Typ nájemného:  </td><td>".$flatTypeMoney."  </td>
  </tr>


    <tr> <td colspan='4'><strong>Subjekt:</strong> ".$row['fm_street'].",  ".$row['fm_town'].",  ".$row['fm_zip']." 
    </td></tr>
    </table>
";

 
$txt1 .="<h4>Seznam ploch</h4>
<table class='singleline'  >
    <tr >
        <td style='width: 25%;    '>Název</td>
        <td style='width: 15%;  '>Podlahová plocha</td>
        <td style='width: 15%; '>Koef.</td>
        <td style='width: 15%; '>Započtená plocha</td>
        <td style='width: 15%;  '>Vytápěná plocha</td>
        <td style='width: 15%;  '>Plocha pro TUV</td>
    </tr>
    <tr>
        <td>obytná plocha</td>
        <td>".$row['flatSquare']." m<sup>2</sup></td>
        <td>1</td>
        <td>".$row['flatSquare']." m<sup>2</sup></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
    <th>Celkem: </th>
    <th>".$row['flatSquare']." m<sup>2</sup></th>
    <th>1</th>
    <th>".$row['flatSquare']." m<sup>2</sup></th>
    <th></th>
    <th></th>
</tr>
</table>";

 $friendsQuery = "SELECT * FROM  fm_persons  WHERE  personFlat = ".$row['personFlat']." And (personTo > now() OR  personTo = '0000-00-00'   )  AND  personHouse  = ".$house." And ID != ".$myid;

 $friendsQuery = "SELECT * FROM fm_persons,h".$house."persons WHERE h".$house."persons.personID = fm_persons.ID And flatID = $row[FLATID] And ( (h".$house."persons.personFrom < now()) And ( h".$house."persons.personTo > now() OR h".$house."persons.personTo = '0000-00-00' )) AND personHouse = ".$house." And personOwner IN (1,2,3)";

  $friendsResult = $GLOBALS["link"]->query($friendsQuery);
  if ($friendsResult && mysqli_num_rows($friendsResult) > 0) {

    $txt1 .="<h4>Seznam spolubydlících osob</h4>
    <table class='singleline' >
        <tr>
            <td style='width: 25%'>Příjmení a jméno</td>
            <td style='width: 15%'>Přihlášen</td>
            <td style='width: 15%'>Odhlášen</td>
            <td style='width: 15%'>Stav / Smlouva</td>
            <td style='width: 15%'></td>
            <td style='width: 15%'>RČ/Dat. narození</td>
        </tr>";
      $osob = 1;
      while ($friendsRow = mysqli_fetch_array($friendsResult)) {
        $osob++;
        $txt1 .="    <tr>
        <td>".$friendsRow['personName']." ".$friendsRow['personSurname']." </td>
        <td>".getDateFromSQL($friendsRow['personFrom'])." </td>
        <td>".getDateFromSQL($friendsRow['personTo'])." </td>
        <td>".$friendsRow['personID']." </td>
        <td></td>
        <td></td>
       </tr>";


      }
      $txt1 .="<tr>
      <th colspan='6'>Celkem počet osob pro služby: ".mysqli_num_rows($friendsResult)."</th>
          </tr>
      </table>  "; 
    }
    elseif(  $row['personFamily'] > 0) {
            $txt1 .="<h4>Seznam spolubydlících osob</h4>
    <table class='singleline' >
        <tr>
            <td style='width: 25%'>Příjmení a jméno</td>
            <td style='width: 15%'>Přihlášen</td>
            <td style='width: 15%'>Odhlášen</td>
            <td style='width: 15%'>Stav / Smlouva</td>
            <td style='width: 15%'></td>
            <td style='width: 15%'>RČ/Dat. narození</td>
        </tr>";
      $osob = $row['personFamily'] + 1;
      $txt1 .="<tr>
      <th colspan='6'>Celkem počet osob pro služby: ".$osob."</th>
          </tr>
      </table>  "; 
       
    }
else{
    $txt1 .="<h4>Seznam spolubydlících osob</h4>
    <table class='singleline' > <tr>
      <th >Celkem počet osob pro služby: 1</th>
          </tr>
      </table>  "; 

}


 $moneyQuery = "SELECT *,fm_services.ID as FID FROM h".$house."money,fm_services WHERE   moneyPublic = 1 And  `moneyFlat` = ".$row['FLATID']." And fm_services.ID = h".$house."money.moneyService AND ( `moneyEnd` = '0000-00-00' OR `moneyEnd` > NOW() ) Order by fm_services.serviceName";
   
   
 
   
     $moneyResult = $GLOBALS["link"]->query($moneyQuery);
     if ($moneyResult && mysqli_num_rows($moneyResult) > 0) {

$txt1 .="<h4>Rozpis ekonomicky oprávněných složek a záloh na služby</h4>
<table class='singleline'  >
    <tr>
        <td style='width: 40%'>Název</td>
        <td style='width: 15%'>Platí od</td>
        <td style='width: 15%'>Platí do</td>
        <td style='width: 15%'>Částka</td>
        <td style='width: 15%'>Typ</td>
    </tr>";

        while ($moneyRow = mysqli_fetch_array($moneyResult)) {

        if( ($moneyRow['FID'] == 8)||($moneyRow['FID'] == 29) ) {  $typ = "Nájem";  } else {  $typ = "Záloha";  }

            $txt1 .="
                 <tr>
                    <td>".$moneyRow['serviceName'] ."</td>
                    <td>".getDateFromSQL($moneyRow['moneyDateFrom'])." </td>
                    <td>".getDateFromSQL($moneyRow['moneyEnd'])."</td>
                    <td style='right'>".$moneyRow['moneyFix'] ." Kč</td>
                    <td>".$typ."</td>
                </tr>";

            $total = $total + $moneyRow['moneyFix'];
        }
    }
    
    $txt1 .= "
    <tr>
        <th colspan='3'><h4>CELKEM MĚSÍČNÍ PLATBA VČETNĚ ZÁLOH NA SLUŽBY: </h4></th>
        <th colspan='3'><h4>".$total." Kč</h4></th>
    </tr></table>

   <p>&nbsp;</p>
     <p>
     Potvrzuji správnost údajů v tomto evidenčním listě a jsem si vědom povinnosti neprodleně a písemně hlásit veškeré změny  v&nbsp;údajích tohoto evidenčního listu, zejména pak změnu počtu osob, užívajících výše jmenovanou jednotku (i dočasně, nejméně 3 měsíce v roce). </p>  
     <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
     <table>
     <tr>
     <td style='border-top: 1px solid #e7e7e7; width:25% ; text-align: center'>Za správce<br/>Panstav služby s.r.o.</td>
     <td></td>
     <td style='border-top: 1px solid #e7e7e7;width: 25%; text-align: center'>Podpis nájemce bytu</td>
     </tr>
     </table>
     
     "; ?>