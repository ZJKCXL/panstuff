<style>
        .bignebig{
         display: block;
         float: left;
         margin-top: 0px;
         width: auto;
        }
        .bigneg{
         width: 200px;
         margin-top: 4px;
 
        }  
        .lft {
            float: left;
            margin-right: 15px;
            display: inline ;
        }
  .deletecomp {
    display: block;
    float: right;
    background: red;
    color: #fff;
    padding: 5px 15px;
   }  
   .deletecomp:hover{
       color: #fff;
      background: #b53b3b
   }
#exampleFormControlSelect2_chosen {
    width: calc(100% - 200px)!important ;
}
  

</style>

<script>

$('.chosen-select').chosen();
$.validator.setDefaults({ ignore: ":hidden:not(select)" });

// validation of chosen on change
if ($("select.chosen-select").length > 0) {
    $("select.chosen-select").each(function() {
        if ($(this).attr('required') !== undefined) {
            $(this).on("change", function() {
                $(this).valid();
            });
        }
    });
}

$('#kontakt').validate({
    errorPlacement: function (error, element) {
        console.log("placement");
        if (element.is("select.chosen-select")) {
            console.log("placement for chosen");
            // placement for chosen
            element.next("div.chzn-container").append(error);
        } else {
            // standard placement
            error.insertAfter(element);
        }
    }
});
</script>

<?php
 
$page_name = "osoby";

if (isset($_REQUEST["newsid"]) && $_REQUEST["newsid"]>0)
{
    $newsid = $_REQUEST["newsid"];
}
else
{
    $newsid = false;
}


if ($newsid)
{
    $query = "SELECT * FROM ".$table_name." WHERE ID =".$newsid;
   
    $res = $GLOBALS["link"]->query($query);
    if ($res && @mysqli_num_rows($res)>0)
    {
        $row = mysqli_fetch_array($res);
    }
}



?>

<h1>Hromadné vkládání nájemníků</h1>
 
<form ENCTYPE="multipart/form-data" action="index.php?id=<?php echo $page_name; ?><?php if($newsid>0){echo "&newsid=",$newsid;}?>" method="post" name="noname" id="kontakt">
 
<div class="row">
  <fieldset class="col-lg-6  col-sm-12"> 

 
  <div class="alert alert-primary" role="alert">
  Do každého řádku vložte údaj o jedné bytové jednotce. V pořadí <strong>Jméno, Číslo jednotky, Počet osob v jednotce a plocha bytu</strong> (pouze číslo). Tyto položky <strong>oddělujte středíkem</strong>.
</div>
 

 <textarea style='width: 100%' rows="10" name='groupdata'>
 
 </textarea>



     



 
 <div style='clear: left;'></div>  
 </fieldset>
 <fieldset class="col-lg-6  col-sm-12"> 

 <?php

echo bootInput('Bydlí od:','required','date',$row['personFrom'],'personFrom',$js); 
 
 

 ?> 

<div class="input-group mb-3">
			<div class="input-group-prepend">
			<span class="input-group-text" id="inputGroup-sizing-default">Dům:</span>
			</div>
			<select  class="form-control  chosen-select  " required id="exampleFormControlSelect2" name="myHouses" >
            <option value='' >vyber</option>
			  <?php
		      $sportSelect = "Select * From fm_house   Order by fm_nickname ";
			  $sportRes = $GLOBALS["link"]->query($sportSelect);
	       	  if ($sportRes && mysqli_num_rows($sportRes) > 0) {			
					while ($sRow = mysqli_fetch_array($sportRes)) {	  

                    if (in_array($sRow['ID'], $mySErv)) { $checked = ' selected '; } else { $checked = '';  }

					?>
					<option value='<?php echo $sRow['ID']; ?>' <?php echo  $checked; ?>  ><?php echo $sRow['fm_nickname']; ?></option>
					<?php
					}
				}
				?>
			
    		</select>
</div>

 
 

</fieldset>


</div>



 
<input type="submit" value="Vložit hromadně " name="group" class="btn btn-primary  " /> 
                                      
</form>
