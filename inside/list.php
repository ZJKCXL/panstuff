
<h1>Domy - zákl. nastavení</h1> 

<script>
var toChange = {
  selector: '.tinymce-2change',
  entity_encoding : "raw",
  menubar: false,
  inline: true,
  plugins: "save",
  toolbar: false,
  setup: function (editor) {
        editor.on('change', function () {
           //  editor.save();
           content = editor.getContent();
           //console.log('save >' + editor.id  + '>' + content);
           $.ajax({
             type:       'POST',
             cache:      false,
             url:        '/inside/ajax-save.php?name=' + editor.id,
             data:       'tinydata=' + content,
             success:    function(dt, status, request) {
               // console.log(request.getAllResponseHeaders());
            }
        });
        });
    }
};
tinymce.init(toChange);
</script>


<?php
 /*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
 */
$info_text = "";

$time = time();
$file_name = date("d_m_y", $time) . "TZ" . $time;
$file_name2 = date("d_m_y", $time) . "TZ" . $time;
$table_name = "fm_house";
$page_name = "list";
$deletext = "Položka byla smazána.";
$updatext = "Položka byla opravena.";
$addtext = "Položka byla přidána.";
$jeho = "Položku";
 //var_dump($_POST);
?>
 
<a href="index.php?id=<?php echo $page_name; ?>new<?php if ($type) {echo "&amp;type=" . $type;}?>" class="fas fa-plus-circle"><span>Přidat další</span></a>
<p>&nbsp;</p>
<?php

if ($_REQUEST['deletemecomp'] > 0) {

    $delquery = 'UPDATE `".$table_name."` SET `".$table_name."`.`servicePublic` = 0  WHERE `".$table_name."`.`ID` = ' . $_REQUEST[deletemecomp] . ' LIMIT 1';
    $delres =  $GLOBALS["link"]->query($delquery);
}
if ($_REQUEST['delete'] > 0) {
     $delquery = 'UPDATE `".$table_name."` SET `".$table_name."`.`servicePublic` = 0  WHERE `".$table_name."`.`ID` = ' . $_REQUEST[delete] . ' LIMIT 1';
    $delres = $GLOBALS["link"]->query($delquery);
}
$supercount = 0;

if (isset($_POST["mkdb"]) && isset($_REQUEST["newsid"]) && is_numeric($_REQUEST["newsid"]) && $_REQUEST["newsid"] > 0) {
    /* byty */
    $settleQuery = "DROP TABLE IF EXISTS `h".$_REQUEST["newsid"]."flats` ";
    $settleQuery1 = " 
    CREATE TABLE `h".$_REQUEST["newsid"]."flats` (
      `ID` int(11) NOT NULL,
      `flatOwner` int(11) NOT NULL COMMENT 'Vlastník bytu',
      `flatPerson` int(11) NOT NULL COMMENT 'ID najemnika bytu',
      `flatSquare` float NOT NULL COMMENT 'Rozmer bytu',
      `flatOccupancy` tinyint(4) NOT NULL DEFAULT 1,
      `flatDeal` float NOT NULL COMMENT 'Podil vlastnika/bytu v proc',
      `flatNr` varchar(10) COLLATE utf8_czech_ci NOT NULL,
      `flatLevel` TINYINT NOT NULL ,
      `flatVS` int(11) NOT NULL,
      `flatLift` tinyint(4) NOT NULL DEFAULT 1,
      `flatTypeFlat` VARCHAR(100) NOT NULL , 
      `flatTypeMoney` VARCHAR(100) NOT NULL ,
      `flatPayDate` VARCHAR(100) NOT NULL ,
      `flatOrder` int(11) NOT NULL
    ) 
    ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci COMMENT='Tabulka bytů v domě číslo ID' ";
    $settleQuery2 = "ALTER TABLE `h".$_REQUEST["newsid"]."flats`   ADD PRIMARY KEY (`ID`)";
    $settleQuery3 = "ALTER TABLE `h".$_REQUEST["newsid"]."flats`   MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT";
    $res  = $GLOBALS["link"]->query($settleQuery);		
    $res1 = $GLOBALS["link"]->query($settleQuery1);	 	
    echo mysqli_error($GLOBALS["link"]);
    $res2 = $GLOBALS["link"]->query($settleQuery2);	
    $res3 = $GLOBALS["link"]->query($settleQuery3);	
    
    /*charges */
    $settleQuery = "DROP TABLE IF EXISTS `h".$_REQUEST["newsid"]."charges` ";
    $settleQuery1 = " 
    CREATE TABLE `h".$_REQUEST["newsid"]."charges` (
        `ID` int(11) NOT NULL,
        `chargesID` bigint(20) NOT NULL,
        `chargesAmount` float NOT NULL,
        `chargesIN` float NOT NULL,
        `chargesOUT` float NOT NULL,
        `chargesFlat` int(11) NOT NULL,
        `chargesDeal` float NOT NULL,        
        `chargesDate` date NOT NULL DEFAULT current_timestamp(),
        `chargesService` smallint(6) DEFAULT NULL,
        `chargesStyle` smallint(6) NOT NULL,
        `chargesNote` varchar(440) COLLATE utf8_czech_ci NOT NULL,
        `chargesUnit` varchar(100) COLLATE utf8_czech_ci NOT NULL,
        `chargesUnitTotal` float NOT NULL,
        `chargesTotal` float NOT NULL,
        `chargesTime` timestamp NOT NULL DEFAULT current_timestamp(),
        `chargesPerson`   int(11) NOT NULL 
    ) 
    ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci COMMENT='Předpisy' ";
    $settleQuery2 = "ALTER TABLE `h".$_REQUEST["newsid"]."charges`   ADD PRIMARY KEY (`ID`)";
    $settleQuery3 = "ALTER TABLE `h".$_REQUEST["newsid"]."charges`   MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT";
    $res  = $GLOBALS["link"]->query($settleQuery);		
    $res1 = $GLOBALS["link"]->query($settleQuery1);	 	
    echo mysqli_error($GLOBALS["link"]);
    $res2 = $GLOBALS["link"]->query($settleQuery2);	
    $res3 = $GLOBALS["link"]->query($settleQuery3);	 


    /*invoices */
    $settleQuery = "DROP TABLE IF EXISTS `h".$_REQUEST["newsid"]."invoices` ";
      $settleQuery1 = " 
    CREATE TABLE `h".$_REQUEST["newsid"]."invoices` (
        `ID` int(11) NOT NULL,
        `chargesOUT` float NOT NULL,
        `chargesDate` date NOT NULL DEFAULT current_timestamp(),
        `chargesService` smallint(6) DEFAULT NULL,
        `chargesStyle` smallint(6) NOT NULL,
        `chargesType` smallint(6) NOT NULL,
        `chargesNote` varchar(440) COLLATE utf8_czech_ci NOT NULL,
        `chargesUnit` varchar(100) COLLATE utf8_czech_ci NOT NULL,
        `chargesUnitTotal` float NOT NULL,
        `chargesTotal` float NOT NULL,
        `chargesTime` timestamp NOT NULL DEFAULT current_timestamp(),
        `chargesOnedrive` varchar(440) COLLATE utf8_czech_ci NOT NULL 
    ) 
    ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci COMMENT='Předpis plateb v domě'";
    $settleQuery2 = "ALTER TABLE `h".$_REQUEST["newsid"]."invoices`   ADD PRIMARY KEY (`ID`)";
    $settleQuery3 = "ALTER TABLE `h".$_REQUEST["newsid"]."invoices`   MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT";
    $res  = $GLOBALS["link"]->query($settleQuery);		
    $res1 = $GLOBALS["link"]->query($settleQuery1);	 	
    echo mysqli_error($GLOBALS["link"]);
    $res2 = $GLOBALS["link"]->query($settleQuery2);	
    $res3 = $GLOBALS["link"]->query($settleQuery3);	 

    /*money */
    $settleQuery = "DROP TABLE IF EXISTS `h".$_REQUEST["newsid"]."money` ";
    $settleQuery1 = " 
    CREATE TABLE `h".$_REQUEST["newsid"]."money` (
        `ID` bigint(20) NOT NULL,
        `moneyFlat` int(11) NOT NULL COMMENT 'ID bytu',
        `moneyService` int(11) NOT NULL COMMENT 'IDsluzby',
        `moneyDateFrom` date NOT NULL DEFAULT current_timestamp(),
        `moneyType` mediumint(9) NOT NULL COMMENT '1|fix, 2|na byt, 3|na osobu, 4|na metry',
        `moneyFix` float NOT NULL,
        `moneyAmount` float NOT NULL,
        `moneyPublic` tinyint(4) NOT NULL DEFAULT 1,
        `moneyNote` varchar(440) COLLATE utf8_czech_ci NOT NULL,
        `moneyMonths` varchar(440) COLLATE utf8_czech_ci NOT NULL,
        `moneyPerson` bigint(20) NOT NULL,
        `moneyOccupancy` tinyint(4) NOT NULL,
        `moneyEnd` date 
    ) 
    ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci";
    $settleQuery2 = "ALTER TABLE `h".$_REQUEST["newsid"]."money`   ADD PRIMARY KEY (`ID`)";
    $settleQuery3 = "ALTER TABLE `h".$_REQUEST["newsid"]."money`   MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT";
    $res  = $GLOBALS["link"]->query($settleQuery);		
    $res1 = $GLOBALS["link"]->query($settleQuery1);	 	
    echo mysqli_error($GLOBALS["link"]);
    $res2 = $GLOBALS["link"]->query($settleQuery2);	
    $res3 = $GLOBALS["link"]->query($settleQuery3);	


}
else{
    
}

if (isset($_REQUEST["send"])) { $poslano = 1; }
if (isset($_REQUEST["sendaback"])) { $poslano = 1; }


if (  ($poslano==1) && isset($_REQUEST["newsid"]) && is_numeric($_REQUEST["newsid"]) && $_REQUEST["newsid"] > 0) {
    if (strlen(trim($_GET["newsid"]))) {
        $query = "Update " . $table_name . " set ";
       
    $superquery = "SHOW FULL COLUMNS FROM `".$table_name."`";
        $superes = $GLOBALS["link"]->query($superquery);
        if ($superes && mysqli_num_rows($superes) > 0) {

        
            while ($supeRow = mysqli_fetch_array($superes)) {

              //  echo "<br/>".$_POST[$supeRow[Field]]  ." |  ".$supeRow[Field];

                if(isset($_POST[$supeRow[Field]])) {
                $col_query  =  $supeRow[Field] ;
                $val_query  =  trim(strip_tags($_POST[$supeRow[Field]])) ;
                if($supercount > 0) { $query .= ", ";  } 
                $supercount++;
                $query .= $col_query . " = '" . $val_query . "'";
                }
   
            }
        }
 
       $query .= " where ID = " . $_REQUEST["newsid"];

        $res = @$GLOBALS["link"]->query($query);
        if ($res && @mysqli_affected_rows($link) > 0) {
            $info_text .= "Položka úspěně uložena.";
            $alertype = " alert-success ";

        } else {
            $info_text .= "Položka nebyla změněna.";
            $alertype = " alert-danger ";
        }

        $goServices = 1; 
        $lastID = $_REQUEST["newsid"]; 

    } else {
        $info_text .= "Chyba během ukládání Položky. Nebyly zadány všechny povinné parametry";
        $alertype = " alert-danger ";
    }

} elseif (($poslano==1)  && !isset($_REQUEST["newsid"])) {

    $query = "INSERT INTO `fm_house` (  `fm_nickname`, `fm_street`, `fm_town`, `fm_zip`
    , `fm_owner`
    , `fm_owner_invoice`
    , `fm_owner_names`
    , `fm_owner_bank`
    , `fm_owner_bankNr`
    , `fm_owner_ICO`
    , `fm_owner_mail`, `fm_owner_phone`, `fm_flats`, `fm_price`, `fm_roundPrice`, `fm_Onedrive`) 
    VALUES ( '".$_POST['fm_nickname']."', '".$_POST['fm_street']."', '".$_POST['fm_town']."', '".$_POST['fm_zip']."'
    , '".$_POST['fm_owner_invoice']."'
    , '".$_POST['fm_owner_names']."'
    , '".$_POST['fm_owner_bank']."'
    , '".$_POST['fm_owner_bankNr']."'
    , '".$_POST['fm_owner_ICO']."'
    
    
    , '".$_POST['fm_owner_mail']."', '".$_POST['fm_owner_phone']."', '".$_POST['fm_flats']."', '".$_POST['fm_price']."', '".$_POST['fm_roundPrice']."', '".$_POST['fm_Onedrive']."')";

    $res = $GLOBALS["link"]->query($query);
    if ($res && mysqli_affected_rows($link) > 0) {
        $info_text .= "Položka úspěně uložena.";
        $alertype = " alert-success ";

        $goServices = 1;
        $lastID = mysqli_insert_id($GLOBALS["link"]); 
 

    } else {
        $info_text .= "Položka nebyla uložena.";
        $alertype = " alert-danger ";
    }

} else {

}

 

if($goServices == 1) {

     $delSport = "Delete From fm_houseVSservices Where vsHouse = ". $lastID;
     $del = $GLOBALS["link"]->query($delSport);	

    foreach ($_POST['myServices'] as $key => $value){
	 $sportquery = "Insert Into fm_houseVSservices (`vsHouse` ,  `vsService` ) values (".$lastID." , ".$value.")  ";
		$respt = $GLOBALS["link"]->query($sportquery);		
	}


}


if (@$info_text != "") {
    ?>
        <div class="alert <?php echo $alertype; ?>" role="alert"><?php echo $info_text; ?></div>
        <?php
}
 


if (isset($_REQUEST["sendaback"])) {
?>
 <script>
 window.location = 'index.php?id=listnew&newsid=<?php echo $newsid; ?>'; 
 </script>

<?php
}
?>

<div class="table-responsive">
<table  id='tableOUT' class="table table-striped table-bordered m-b-0 toggle-circle footable-loaded footable tablet breakpoint demo-inline">

<tr>
    <th style='width: 105px'>Edit</th>
    <th>Smazat</th> 
    <th>Dům</th>
    <th>Bytů</th>
    <th>FA</th>
    <th>Byt Fee</th>
    <th>Fee</th>    
    <th>Real Fee</th>    
    <th>Q Fee</th>    
    <th>Služby</th>    
    <th>Charging</th>   
</tr>
<tbody id='thisTBL' class="row_position" >
<?php
$time = time();
if ($_GET['order'] == 1) {$ordr = 'serviceName';} else { $ordr = 'fm_nickname  ';}

$query = "SELECT * FROM $table_name Where fm_Public = 1 ORDER BY " . $ordr;
$result = $GLOBALS["link"]->query($query);

if ($result && mysqli_num_rows($result) > 0) {
    while ($row = mysqli_fetch_array($result)) {
        ?>
         <tr  id="<?php echo $row['ID']; ?>" >
            <td  class='short center'> 
            <a href='index.php?id=<?php echo $page_name; ?>new&newsid=<?php echo $row['ID']; ?>' class="far fa-edit" ></a>
            </td>
            <td class="short center">
            <a onclick="return confirm('Skutečně chcete položku vymazat z databáze?')" href="index.php?id=<?php echo $page_name; ?>&amp;delete=<?php echo $row['ID']; ?>" class="far fa-times-circle text-danger"></a>
            </td>
            <td class='tinymce-2change' id='fm_nickname--<?php echo $row['ID']; ?>--<?php echo $table_name; ?>'><?php echo $row['fm_nickname']; ?></td>
            <td class='tinymce-2change' id='fm_flats--<?php echo $row['ID']; ?>--<?php echo $table_name; ?>'><?php echo $row['fm_flats']; ?></td>
            <td class='tinymce-2change' id='fm_Period--<?php echo $row['ID']; ?>--<?php echo $table_name; ?>'><?php echo $row['fm_Period']; ?></td>
            <td class='tinymce-2change' id='fm_price--<?php echo $row['ID']; ?>--<?php echo $table_name; ?>'><?php echo $row['fm_price']; ?></td>
            <td><?php echo $fee = $row['fm_price']*$row['fm_flats']; ?></td>                        
            <td class='tinymce-2change' id='fm_roundPrice--<?php echo $row['ID']; ?>--<?php echo $table_name; ?>'><?php echo $row['fm_roundPrice']; ?> </td>          
            <td><?php if($row['fm_Period'] == 'Q') { echo $feeQ = $row['fm_roundPrice']*3; } ?></td>  
            <?php  $totalREALfee = $totalREALfee +  $row['fm_roundPrice']; ?>    
            <?php  $totalfee = $totalfee +  $fee; ?> 
                             
            <td>
            <?php
                $findServices  = "SELECT * FROM `fm_houseVSservices` LEFT JOIN fm_services ON vsService = fm_services.ID WHERE vsHouse =  ".$row['ID'];
                $findRes = $GLOBALS["link"]->query($findServices);
                if ($findRes && mysqli_num_rows($findRes) > 0) {
                    while ($findRow = mysqli_fetch_array($findRes)) {
                       ?>
                     <!--  &nbsp;<i data-toggle="tooltip" data-placement="top" title="<?php echo $findRow['serviceName']; ?>"  class="<?php echo $findRow['serviceIco']; ?> typeICO-<?php echo $findRow['serviceType']; ?>"></i>&nbsp; -->
                       <?php
                    }
                }

 
                 $findServices  = "SELECT * FROM `fm_services` LEFT JOIN fm_houseVSservices ON vsHouse = ".$row['ID']." And vsService = fm_services.ID Order by serviceOrder";
                $findRes = $GLOBALS["link"]->query($findServices);
                if ($findRes && mysqli_num_rows($findRes) > 0) {
                    while ($findRow = mysqli_fetch_array($findRes)) {

                       if($findRow['vsHouse'] == $row['ID']) {
                           $col = $findRow['serviceType'];
                       }
                       else {
                        $col = 'NOT';
                       }

                       ?>
                        <i data-toggle="tooltip" data-placement="top" title="<?php echo $findRow['serviceName']; ?>"  class="<?php echo $findRow['serviceIco']; ?> typeICO-<?php echo $col; ?>"></i> 
                       <?php
                    }
                }


            ?>
            </td>   
            <td><?php
              if($row['flat_Cron'] == 1) {
               ?>
              <i class="far fa-calendar-check" data-toggle="tooltip" data-placement="top" title="Nastaveno pravidelné automatické načítání předpisů" ></i>
               <?php
              }
            ?></td>     
        </tr>
         <?php
}
}
?>
<tr>
    <th>CELKEM</th>
    <th> </th> 
    <th> </th>
    <th> </th>
    <th> </th>
    <th> </th>
    <th><?php echo $totalfee; ?></th>    
    <th><?php echo $totalREALfee; ?></th>   
    <th><?php echo $totalREALfee*3; ?></th>   
    <th> </th> 
    <th><a class='color' href='index.php?id=<?php echo $page_name; ?>'><i class="fas fa-sync"></i> Reload</a></th>  
    
</tr>
</tbody>
</table>
</div>

<a href="#" id="btnExport"> EXCEL </a>

<script type="text/javascript">



    $("#btnExport").click(function (e) {
        var htmltable= document.getElementById('tableOUT');
        var html = htmltable.outerHTML;
        while (html.indexOf('á') != -1) html = html.replace('á', '&aacute;');
        while (html.indexOf('Á') != -1) html = html.replace('Á', '&Aacute;');
        while (html.indexOf('č') != -1) html = html.replace('č', '&#269;');
        while (html.indexOf('Č') != -1) html = html.replace('Č', '&#268;');
        while (html.indexOf('é') != -1) html = html.replace('é', '&eacute;');
        while (html.indexOf('É') != -1) html = html.replace('É', '&Eacute;');
        while (html.indexOf('í') != -1) html = html.replace('í', '&iacute;');
        while (html.indexOf('Í') != -1) html = html.replace('Í', '&Iacute;');
        while (html.indexOf('ó') != -1) html = html.replace('ó', '&oacute;');
        while (html.indexOf('Ó') != -1) html = html.replace('Ó', '&Oacute;');
        while (html.indexOf('ú') != -1) html = html.replace('ú', '&uacute;');
        while (html.indexOf('Ú') != -1) html = html.replace('Ú', '&Uacute;');
        while (html.indexOf('º') != -1) html = html.replace('º', '&ordm;');
        while (html.indexOf('ň') != -1) html = html.replace('ň', '&#328;');
        while (html.indexOf('Ň') != -1) html = html.replace('Ň', '&#327;');
        while (html.indexOf('š') != -1) html = html.replace('š', '&#353;');
        while (html.indexOf('Š') != -1) html = html.replace('Š', '&#352;');
        while (html.indexOf('ř') != -1) html = html.replace('ř', '&#345;');
        while (html.indexOf('Ř') != -1) html = html.replace('Ř', '&#344;');
        while (html.indexOf('ť') != -1) html = html.replace('ť', '&#357;');
        while (html.indexOf('Ť') != -1) html = html.replace('Ť', '&#356;');
        while (html.indexOf('´') != -1) html = html.replace('´', '&#39;');
        while (html.indexOf('ě') != -1) html = html.replace('ě', '&#283;');
        while (html.indexOf('Ě') != -1) html = html.replace('Ě', '&#282;');
        while (html.indexOf('Ý') != -1) html = html.replace('Y', '&Yacute;');
        while (html.indexOf('ý') != -1) html = html.replace('ý', '&yacute;');
        while (html.indexOf('ž') != -1) html = html.replace('ž', '&#382;');
        while (html.indexOf('Ž') != -1) html = html.replace('Ž', '&#381;');
        var result = 'data:application/vnd.ms-excel,' + encodeURIComponent(html);
        this.href = result;
        this.download = "SLUZBY.xls";
        return true;
    });

    $( ".row_position" ).sortable({
        delay: 150,
        stop: function() {
            var selectedData = new Array();
            $('.row_position>tr').each(function() {
                selectedData.push($(this).attr("id"));
            });
            updateOrder(selectedData);
          }
    });
    $( ".xx" ).mouseenter(function() {
        $(".row_position").sortable("enable");
    }).mouseleave(function() {
        $(".row_position").sortable("disable");
    });

    $( ".tinymce-2change" ).mouseenter(function() {
        $(".row_position").sortable("disable");
    });

    function updateOrder(data) {
        console.log(data);
        $.ajax({
            url:'/inside/ajax-order.php?name=' + '<?php echo "serviceOrder--b--" . $table_name; ?>',
            type:'post',
            data:{position:data},
            success:function(){
                console.log(data);
            }
        })
    }


</script>