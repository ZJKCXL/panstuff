<h1>Pokladna</h1> 
<?php include('./modules/warning.php'); ?>

<script>
var toChange = {
  selector: '.tinymce-2change',
  entity_encoding : "raw",
  menubar: false,
  inline: true,
  plugins: "save",
  toolbar: false,
  setup: function (editor) {
        editor.on('change', function () {
           //  editor.save();
           content = editor.getContent();
            console.log('save >' + editor.id  + '>' + content);
           $.ajax({
             type:       'POST',
             cache:      false,
             url:        '/inside/ajax-save.php?name=' + editor.id,
             data:       'tinydata=' + content,
             success:    function(dt, status, request) {
               // console.log(request.getAllResponseHeaders());
            }
        });
        });
    }
};
tinymce.init(toChange);
</script>

<?php
 /*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
 */
$info_text = "";

$time = time();
$file_name = date("d_m_y", $time) . "TZ" . $time;
$file_name2 = date("d_m_y", $time) . "TZ" . $time;
$table_name = "h".$_GET['house']."charges";
$page_name = "payments";
$deletext = "Položka byla smazána.";
$updatext = "Položka byla opravena.";
$addtext = "Položka byla přidána.";
$jeho = "Položku";

//var_dump($_POST);


// clear sanon //

$clearServices = "Select *,prntServices.ID as PSID From prntServices, prntSanon Where prntSanon.sanonTimeID = prntServices.sanonId And prntSanon.sanonLoaded = 0";
$clearRes = @$GLOBALS["link"]->query($clearServices);
if ($clearRes && mysqli_num_rows($clearRes) > 0) {			
    while ($RowClear = mysqli_fetch_array($clearRes)) {	 

        $clearQuery2 = "Delete From prntSanon Where sanonTimeID = " .$RowClear['sanonID'];  
        $clearRes2 = @$GLOBALS["link"]->query($clearQuery2);
          $clearQuery3 = "Delete From prntServices Where  prntServices.ID = " .$RowClear['PSID'];  
        $clearRes3 = @$GLOBALS["link"]->query($clearQuery3);

    }
}
 


?>


<a href="index.php?id=<?php echo $page_name; ?>new<?php if ($_GET['house']) {echo "&amp;house=" . $_GET['house'];}?><?php if ($_GET['service1']) {echo "&amp;service1=" . $_GET['service1'];}?><?php if ($_GET['service2']) {echo "&amp;service2=" . $_GET['service2'];}?>" class="  fas fa-plus-circle"><span>Přidat platbu</span></a>

<a href="index.php?id=<?php echo $page_name; ?>INnew<?php if ($_GET['house']) {echo "&amp;house=" . $_GET['house'];}?><?php if ($service1) {echo "&amp;service1=" . $service1;}?><?php if ($service2) {echo "&amp;service2=" . $service2;}?>" class="  fas fa-plus-circle"><span>Přidat výdaj bytu &nbsp;&nbsp;</span></a>

<p>&nbsp;</p>
 


<form action="index.php?id=<?php echo $page_name; ?>&house=<?php echo $_GET['house']; ?>&service1=<?php echo $service1; ?>&service2=<?php echo $service2; ?>&service3=<?php echo $service3; ?>&service4=<?php echo $service4; ?>" method="get" name="noname" class='tabupper' style='float: left;'>
 
<select name="service1">
<option value="-1">Byt</option>
<?php
 $Select = "Select flatNr,ID From  h".$_GET['house']."flats Order by flatNr+0 ";
 $Res = @$GLOBALS["link"]->query($Select);
if ($Res && mysqli_num_rows($Res) > 0) {			
    while ($Row = mysqli_fetch_array($Res)) {	 
    ?>
    <option <?php if($service1 == $Row['ID']){ echo " selected "; } ?> value="<?php echo $Row['ID']; ?>">Byt č. <?php echo $Row['flatNr']; ?> </option>
    <?php
    }
}
?>
</select>
<input type='hidden' name='house' value='<?php echo $_GET['house']; ?>' />
<input type='hidden' name='id' value='<?php echo $_GET['id']; ?>' />


<select name="owners">
<option value="-1">Byty vlastníka</option>
<?php
 $Select = "SELECT personID FROM `h".$_REQUEST['house']."persons` Where personOwner IN(2,4)  Group by personID     ";
 $Res = @$GLOBALS["link"]->query($Select);
if ($Res && mysqli_num_rows($Res) > 0) {			
    while ($Row = mysqli_fetch_array($Res)) {	 
    ?>
    <option <?php if($owners == $Row['personID']){ echo " selected "; } ?> value="<?php echo $Row['personID']; ?>"><?php echo getPersonByID($Row['personID']); ?></option>
    <?php
    }
}
?>
</select>

<select name="tenants">
<option value="-1">Nájemník</option>
<?php
 $Select = "SELECT personID FROM `h".$_REQUEST['house']."persons` Where personOwner = 1  Group by personID    ";
 $Res = @$GLOBALS["link"]->query($Select);
if ($Res && mysqli_num_rows($Res) > 0) {			
    while ($Row = mysqli_fetch_array($Res)) {	 
    ?>
    <option <?php if($tenants == $Row['personID']){ echo " selected "; } ?> value="<?php echo $Row['personID']; ?>"><?php echo getPersonByID($Row['personID']); ?></option>
    <?php
    }
}
?>
</select>


<?php
 
$rok = date("Y", time());
$rok2 = date("Y", time())-1;
$rok3 = date("Y", time())-2;
$rok4 = date("Y", time())-3;
?>

<select name="service2">
<option value="<?php echo $rok; ?>" <?php if($service2 == $rok){ echo " selected "; } ?> ><?php echo $rok; ?></option>
<option value="<?php echo $rok2; ?>" <?php if($service2 == $rok2){ echo " selected "; } ?> ><?php echo $rok2; ?></option>
<option value="<?php echo $rok3; ?>" <?php if($service2 == $rok3){ echo " selected "; } ?> ><?php echo $rok3; ?></option>
</select>

<select name="service3">
<option value="0" <?php if($service3 == 0){ echo " selected "; } ?> >Vše</option>
<option value="1" <?php if($service3 == 1){ echo " selected "; } ?> >Předpisy a platby</option>
<option value="2" <?php if($service3 == 2){ echo " selected "; } ?> >Platby</option>
<option value="3" <?php if($service3 == 3){ echo " selected "; } ?> >Předpisy</option>
<option value="4" <?php if($service3 == 4){ echo " selected "; } ?> >Náklady</option>
<option value="5" <?php if($service3 == 5){ echo " selected "; } ?> >Náklady a Předpisy služeb</option>
</select>


<select name="service4">
<option value="0" <?php if($service4 == 0){ echo " selected "; } ?> >Všechy služby</option>
<?php


$serviceQuery = 'SELECT *,fm_services.ID as THISID FROM fm_houseVSservices,fm_services  WHERE vsService = fm_services.ID And `vsHouse` =  '.$_GET['house'];
$serviceres = $GLOBALS["link"]->query($serviceQuery);
if ($serviceres && @mysqli_num_rows($serviceres)>0)
{
 while ($srow =  mysqli_fetch_array($serviceres))
 {
   echo "<option value='".$srow['THISID']."'";
   
   if($service4 == $srow['THISID']){ echo " selected "; } 

   echo " >".$srow['serviceName']."</option>";
 }
}

?>
</select>
 

<input type="submit" name="filtr" class="btn btn-secondary btn-sm soupup" value="Filtrovat"/>

</form>
 

<p>&nbsp;</p>
<?php


if(!isset($_REQUEST['service3'])) { $service3 = 2; }

if ($_REQUEST['deletemecomp'] > 0) {

    $delquery = "UPDATE `".$table_name."` SET `".$table_name."`.`moneyPublic` = 0  WHERE `".$table_name."`.`ID` = '".$_REQUEST[deletemecomp]."' LIMIT 1";
    $delres =  $GLOBALS["link"]->query($delquery);
}
if ($_REQUEST['delete'] > 0) {
     $delquery = "DELETE FROM  `".$table_name."`  WHERE `".$table_name."`.`ID` = '".$_REQUEST[delete]."' LIMIT 1";
    $delres = $GLOBALS["link"]->query($delquery);
}
$supercount = 0;

if (isset($_REQUEST["send"]) && isset($_REQUEST["newsid"]) && is_numeric($_REQUEST["newsid"]) && $_REQUEST["newsid"] > 0) {

    if (strlen(trim($_GET["newsid"]))) {
        $upquery = "Update " . $table_name . " set ";
       
        $superquery = "SHOW FULL COLUMNS FROM `".$table_name."`";
        $superes = $GLOBALS["link"]->query($superquery);
        if ($superes && mysqli_num_rows($superes) > 0) {
            $supercount = 0;
            while ($supeRow = mysqli_fetch_array($superes)) {
                if(isset($_POST[$supeRow[Field]])) {
                $col_query  =  $supeRow[Field] ;

                $val_query  =  trim(strip_tags($_POST[$supeRow[Field]])) ;
                if($supercount > 0) { $upquery .= ", ";  } 
                $supercount++;
                $upquery .=  $col_query . " = '" . $val_query . "'";
                }
                $row++;
            }
        }
         $upquery .= " where ID = " . $_REQUEST["newsid"];
         $res = $GLOBALS["link"]->query($upquery);

    } else {
        $info_text .= "Chyba během ukládání Položky. Nebyly zadány všechny povinné parametry";
        $alertype = " alert-danger ";
    }

} elseif (   isset($_REQUEST["send"]) && !isset($_REQUEST["newsid"])) {
   
    if($_POST['multiPAY'] != 'OK') {

   echo "<hr/>". $query = "INSERT INTO ".$table_name."
    (
        `chargesFlat`,
        `chargesService`,
        `chargesDate`,
        `chargesIN`,
        `chargesOUT`,
        `chargesTotal`,
        `chargesUnitTotal`,
        `chargesUnit`,                        
        `chargesNote`,
        `chargesPerson` 
    ) 
    VALUES 
    (
        '".$_POST["chargesFlat"]."',
        '".$_POST["chargesService"]."',
        '".$_POST["chargesDate"]."',
        '".$_POST["chargesIN"]."',
        '".$_POST["chargesOUT"]."',
        '".$_POST["chargesTotal"]."',
        '".$_POST["chargesUnitTotal"]."',
        '".$_POST["chargesUnit"]."',
        '".$_POST["chargesNote"]."' ,
        '".$_POST["chargesPerson"]."' 
    )";

    $res = $GLOBALS["link"]->query($query);
    if ($res && mysqli_affected_rows($link) > 0) {
        $info_text .= "Položka úspěně uložena.";
        $alertype = " alert-success ";
 
    } else {
        $info_text .= "Položka nebyla uložena.";
        $alertype = " alert-danger ";
    }

    }else{
     //  echo $_POST['myMonths'] ;
   // hromadny zadavani

   foreach ($_POST['myMonths'] as $key => $value){  
          
          $datumka = explode ('-',$_POST["chargesDate"]);
          $dateIN =   $datumka[0]."-".$value."-".$datumka[2];

           $query = "INSERT INTO ".$table_name."
          (
              `chargesFlat`,
              `chargesService`,
              `chargesDate`,
              `chargesIN`,
              `chargesOUT`,
              `chargesTotal`,
              `chargesUnitTotal`,
              `chargesUnit`,                        
              `chargesNote`,
              `chargesPerson` 
          ) 
          VALUES 
          (
              '".$_POST["chargesFlat"]."',
              '".$_POST["chargesService"]."',
              '".$dateIN."',
              '".$_POST["chargesIN"]."',
              '".$_POST["chargesOUT"]."',
              '".$_POST["chargesTotal"]."',
              '".$_POST["chargesUnitTotal"]."',
              '".$_POST["chargesUnit"]."',
              '".$_POST["chargesNote"]."' ,
              '".$_POST["chargesPerson"]."' 
          )";
      
          $res = $GLOBALS["link"]->query($query);



        }
   


  // end hromadny zadavani  

    }

} else {

}

if($_GET['copy'] > 0) {

  $copyQuery = "insert into h1money( `moneyFlat`,`moneyService`, `moneyType`, `moneyFix` ) select  `moneyFlat` ,`moneyService`, `moneyType`, `moneyFix` from h1money where ID= ".$_GET['copy'];
 $res = $GLOBALS["link"]->query($copyQuery);

}

if (isset($_REQUEST["multisend"]))   {

$invoiceQuery = "INSERT INTO `h11invoices` 
(`chargesOUT`, `chargesDate`, `chargesService`, `chargesStyle`, `chargesNote`, `chargesUnit`, `chargesUnitTotal`, `chargesTotal`) 
VALUES 
('".$_POST['chargesOUT']."', '".$_POST['chargesDate']."', '".$_POST["chargesService"]."', '".$_POST['moneyType']."', '".$_POST['chargesNote']."', '".$_POST['chargesUnit']."', '".$_POST['chargesUnitTotal']."', '".$_POST['chargesOUT']."')";

$invoiceRes = @$GLOBALS["link"]->query($invoiceQuery);
$lastID = mysqli_insert_id($GLOBALS["link"]);
 
}
 
if (isset($_REQUEST["multisend"]))   {

    $bytuSelect = "Select count(*) as BYTU From  h".$_GET['house']."flats";
$bytuRes = @$GLOBALS["link"]->query($bytuSelect);
if ($bytuRes && mysqli_num_rows($bytuRes) > 0) {			
    while ($bytuRow = mysqli_fetch_array($bytuRes)) {	  
         $bytu =  $bytuRow['BYTU'];
    }
}

$bytuSelect = "Select count(*) as BYTU From  h".$_GET['house']."flats  Where flatLift = 1 ";
$bytuRes = @$GLOBALS["link"]->query($bytuSelect);
if ($bytuRes && mysqli_num_rows($bytuRes) > 0) {			
    while ($bytuRow = mysqli_fetch_array($bytuRes)) {	  
         $liftbytu =  $bytuRow['BYTU'];
    }
}

$bytuSelect = "Select sum(flatOccupancy) as LIDU From  h".$_GET['house']."flats";
$bytuRes = @$GLOBALS["link"]->query($bytuSelect);
if ($bytuRes && mysqli_num_rows($bytuRes) > 0) {			
    while ($bytuRow = mysqli_fetch_array($bytuRes)) {	  
           $lidu =  $bytuRow['LIDU'];
    }
} 

$bytuSelect = "Select sum(flatSquare) as METRU From  h".$_GET['house']."flats";
$bytuRes = @$GLOBALS["link"]->query($bytuSelect);
if ($bytuRes && mysqli_num_rows($bytuRes) > 0) {			
    while ($bytuRow = mysqli_fetch_array($bytuRes)) {	  
           $metru =  $bytuRow['METRU'];
    }
} 

$flatsSelect = "SELECT * FROM  h".$_GET['house']."flats";
$flatsRes = @$GLOBALS["link"]->query($flatsSelect);
if ($flatsRes && mysqli_num_rows($flatsRes) > 0) {			
    while ($flatsRow = mysqli_fetch_array($flatsRes)) {	

   
  if( $_POST["chargesService"] == 24 ) { 
 
    $amount =   $_POST['chargesOUT']/$liftbytu  ;

  }
  else{
 
 if($_POST['moneyType'] == 2) { $jak = 'Na byt'; $amount = $_POST['chargesOUT']/$bytu; } 
 elseif($_POST['moneyType'] == 3) { $jak = 'Na osobu ('.$flatsRow['flatOccupancy'].')';   $amount = ($_POST['chargesOUT']/$lidu)*$flatsRow['flatOccupancy']  ;} 
 elseif($_POST['moneyType'] == 4) { $jak = 'Na metry ('.$flatsRow['flatSquare'].'m<sup>2</sup>)';  $amount = ($_POST['chargesOUT']/$metru)*$flatsRow['flatSquare']  ;} 
 else { $jak = 'Fix';  $amount = $_POST['chargesOUT']; }

  }

$flatsRow['flatLift']." | ".$_POST["chargesService"] ;

if( (($flatsRow['flatLift'] == 1) && ( $_POST["chargesService"] == 24 )) ||  ( $_POST["chargesService"] != 24 ))  {

echo $query = "INSERT INTO ".$table_name."
  (

     `chargesFlat`,
     `chargesService`,
     `chargesDate`,
     `chargesOUT`,
     `chargesTotal`,
     `chargesUnitTotal`,
     `chargesUnit`,                        
     `chargesNote`

     ) 
   VALUES 
   (
     
    '".$flatsRow['flatNr']."',
    '".$_POST["chargesService"]."',
    '".$_POST["chargesDate"]."',
    '".$amount."',
    '".$_POST["chargesOUT"]."',
    '".$_POST["chargesUnitTotal"]."',
    '".$_POST["chargesUnit"]."',
    '".$_POST["chargesNote"]."' 

)";



 $res = $GLOBALS["link"]->query($query);

   }


    }
}


}


if (@$info_text != "") {
    ?>
        <div class="alert <?php echo $alertype; ?>" role="alert"><?php echo $info_text; ?></div>
        <?php
}
?>
<div class="table-responsive">
<table  id='tableOUT' class="table table-bordered m-b-0 toggle-circle footable-loaded footable tablet breakpoint demo-inline">
<thead>
<tr>
    <th>Edit</th>
    <th>Zrušit</th>
    <th>Byt</th>
    <th>  <a href='/index.php?id=money&house=<?php echo $_GET['house']; ?>&service1=<?php echo $_GET['service1']; ?>'>Předpisy</a> </th>
    <th>Přijato</th>
    <th>Reálný výdaj</th>
    <th>Služba</th>
    <th>Kdy</th>
    <th>Osoba</th>
    <th>Poznámka</th>
    
</tr>
</thead>
<tbody id='thisTBL' class="row_position" >

<?php
$time = time();

 
if($service1 > 0) {
    $where1 = " And chargesFlat = '".$service1."'" ; 
    $persWhere1 = " And historyFlatID = '".$service1."'" ; 
}
 
if($service2 > 0) {

    $where2 = " And chargesDate  >= '".$service2."-01-01'  And  chargesDate  <=  '".$service2."-12-31'    "    ;   
    $persWhere2 = " And historyDate  >= '".$service2."-01-01'  And  historyDate  <=  '".$service2."-12-31'    "    ;   
    
     
}
 
if($service3 == 1) {
    $where3 = " And ( ( chargesIN > 0 ) OR ( chargesAmount > 0  ))  "    ;  
}
if($service3 == 2) {
    $where3 = " And ( chargesIN > 0  )  "    ;  
}
if($service3 == 3) {
    $where3 = " And ( chargesAmount > 0  )  "    ;  
}
if($service3 == 4) {
    $where3 = " And ( chargesOUT > 0  )  "    ;  
}
if($service3 == 5) {
    $where3 = " And ( ( chargesOUT > 0 ) OR (( chargesAmount > 0  ) And ( chargesService = 27 ))  )  "    ;  
}
 

if($service4 > 0) {

    $where4 = " And chargesService    =  ".$service4." "     ;   
     
}

if($_REQUEST['owners']>0) {

    //$whereOwn =     " And flatOwner =   ".$_REQUEST['owners'] ;
    $persWhereOwn = " And historyOwner =   ".$_REQUEST['owners'] ;
    $whereOwn =     " And h".$_GET['house']."persons.personID   =   ".$_REQUEST['owners']. " And personOwner = 4 " ;


}

if($_REQUEST['tenants']>0) {

  //  $whereTen = "     And chargesPerson =   ".$_REQUEST['tenants'] ;
    $persWhereTen = " And historyPerson =   ".$_REQUEST['tenants'] ;
    $whereTen =     " And h".$_GET['house']."persons.personID   =   ".$_REQUEST['tenants']. " And personOwner = 1 " ;

}



$serviceQuery = 'SELECT *,fm_services.ID as THISID FROM fm_houseVSservices,fm_services  WHERE vsService = fm_services.ID And `vsHouse` =  '.$_GET['house'];
$serviceres = $GLOBALS["link"]->query($serviceQuery);
if ($serviceres && mysqli_num_rows($serviceres) > 0) {
    while ($serviceRow = mysqli_fetch_array($serviceres)) {
          $SERVIS[$serviceRow['THISID']] = $serviceRow['serviceName'];
    }
} 

  $query = "SELECT *,".$table_name.".ID as CHID FROM ".$table_name.",h".$_GET['house']."flats, fm_persons Where   fm_persons.ID =  chargesPerson And    h".$_GET['house']."flats.ID = ".$table_name.".chargesFlat  ".$where1.$where2.$where3.$where4.$whereOwn.$whereTen."  Order by chargesDate DESC, chargesTime DESC  " ;
$result = $GLOBALS["link"]->query($query);

 $query = "SELECT *,".$table_name.".ID as CHID, h".$_GET['house']."flats.ID as FID FROM ".$table_name.",h".$_GET['house']."flats, h".$_GET['house']."persons Where h".$_GET['house']."persons.flatID =  h".$_GET['house']."flats.ID  And    h".$_GET['house']."flats.ID = ".$table_name.".chargesFlat  ".$where1.$where2.$where3.$where4.$whereOwn.$whereTen."  Group by ".$table_name.".ID  Order by chargesDate DESC, chargesTime DESC ";
$result = $GLOBALS["link"]->query($query);

 $query = "SELECT *,".$table_name.".ID as CHID   FROM ".$table_name.",h".$_GET['house']."persons   WHERE  chargesFlat = flatID   And 	(personTo = '0000-00-00' OR personTo > `chargesDate` ) And   (personFrom < `chargesDate` + INTERVAL 1 DAY )  ".$where1.$where2.$where3.$where4.$whereOwn.$whereTen."  Group by ".$table_name.".ID  Order by chargesDate DESC, chargesTime DESC ";

$flatSquare = $totalDeal = 0;

 

if ($result && mysqli_num_rows($result) > 0) {

    $totalAmount = 0;
    $totalIN = 0;
    $totalSERVICE = 0;
    $radek = 0;
    while ($row = mysqli_fetch_array($result)) {
        $radek++;
        
       if($row['chargesAmount'] == 0) { $chA  = ''; $dateA = '';} else {  $chA = number_format($row['chargesAmount'],2,',','&nbsp;'); $mydate = $dateA = getDateFromSQL($row['chargesDate']); }
       if($row['chargesIN'] == 0) { $chIN  = '';  $dateIN = '';} else {  $chIN = number_format($row['chargesIN'],2,',','&nbsp;');  $mydate = $dateIN = getDateFromSQL($row['chargesDate']);}
       if($row['chargesOUT'] == 0) { $chOUT  = '';  $dateOUT = '';} else {  $chOUT = number_format($row['chargesOUT'],2,',','&nbsp;');  $mydate = $dateOUT = getDateFromSQL($row['chargesDate']);}

       if(($row['chargesAmount'] != 0)&&($row['chargesService'] == 27)) { $CHSERVICE = $row['chargesAmount']; } else { $CHSERVICE = 0;   }


       if($row['chargesAmount'] > 0) { $trCLR = '#ebeded5e';  }
       elseif($row['chargesIN'] > 0) {  $trCLR = '#eaf4e4';}
       else {$trCLR = '#feecf0';  }

       $totalAmount = $totalAmount + $row['chargesAmount'];
       $totalIN = $totalIN + $row['chargesIN'];
       $totalOUT = $totalOUT + $row['chargesOUT'];
       $totalSERVICE = $totalSERVICE + $CHSERVICE;

       $partServices[$row['chargesService']] = $partServices[$row['chargesService']] + $row['chargesOUT'] ;

       if(($row['chargesService'] == 8)||($row['chargesService'] == 9)||($row['chargesService'] == 29)) {
            $totalnajem = $totalnajem + $row['chargesAmount'];
       }

        ?>
        <tr id="<?php echo $row['THISID']; ?>" style='background: <?php echo $trCLR; ?>'>
        <td  class='short center' ><a href='index.php?id=<?php echo $page_name; ?>new&allTimes=<?php echo $_REQUEST['allTimes']; ?>&service1=<?php echo $service1; ?>&service2=<?php echo $service2; ?>&service3=<?php echo $service3; ?>&newsid=<?php echo $row['CHID']; ?>&house=<?php echo $_REQUEST['house']; ?>' class="far fa-edit" ></a>
        <td class="short center"><a onclick="return confirm('Skutečně chcete položku vymazat z databáze?')" href="index.php?id=<?php echo $page_name; ?>&service1=<?php echo $service1; ?>&service2=<?php echo $service2; ?>&service3=<?php echo $service3; ?>&house=<?php echo $_REQUEST['house']; ?>&amp;delete=<?php echo $row['CHID']; ?>" class="far fa-times-circle text-danger"></a></td>
            
        <td class='short center'><?php echo $row['flatNr']; ?></td>        
        <td class='right  tinymce-2change' id='chargesAmount--<?php echo $row['CHID']; ?>--<?php echo $table_name; ?>'><?php echo $chA; ?></td>

        <td class='right'><?php echo $chIN; ?></td>
        <td class='right'><?php echo $chOUT; ?></td>
        <td><?php echo $SERVIS[$row['chargesService']]; ?></td>
        <td> <?php echo $mydate; ?>   </td>    
        <td  class='right'>  
         <!-- <?php if($row['chargesTotal'] > 0)  { ?> <?php echo number_format($row['chargesTotal'],2,',','&nbsp;');  ?> <?php }  ?>
          <?php if($row['chargesUnitTotal'] > 0)  { ?>  |  <?php echo $row['chargesUnitTotal']; ?> <?php echo $row['chargesUnit'];   ?>  <?php }  ?>-->
          <?php   $row['personSurname']; ?>&nbsp;<?php   $row['personName']; ?>
          
          <?php echo getPersonByID(getActualTenant($row['chargesDate'],$row['FID'],$house));  ?>
          
          &nbsp;<?php
            if(($row['chargesDeal']>0) &&($row['chargesAmount']>0))  
            { 
                echo  "(".$row['chargesDeal'].")";  
                 
            }             
          ?></td>  
        <td class='   tinymce-2change' id='chargesNote--<?php echo $row['CHID']; ?>--<?php echo $table_name; ?>' ><?php echo $row['chargesNote']; ?></td>
        
        </tr>
         <?php
}
}
?>
 
 </tbody>
 
 <tr style='background: #ebee84'>
   <th>CELKEM</th>
   <th> </th>
   <th  colspan="2" data-toggle="tooltip" data-placement="top" title="Celkem předpisy"  class='right'><?php echo number_format($totalAmount,2,',','&nbsp;'); ?> </th>
 
   <th   data-toggle="tooltip" data-placement="top" title="Celkem přijaté úhrady"  class='right'><?php echo number_format($totalIN,2,',','&nbsp;'); ?></th>
 
   <th   data-toggle="tooltip" data-placement="top" title="Celkem rozpočítané náklady"  class='right'><?php echo number_format($totalOUT,2,',','&nbsp;'); ?></th>
   <td></td>
   <td></td>
   <td></td>
   <td> </td>
   
 </tr>


 <tr>
   <th>Nájem/FO</th>
   <th> </th>
<?php

$nasluzby = $totalIN - $totalnajem;

?>
   <th  colspan="2" data-toggle="tooltip" data-placement="top" title="Celkem předepsaný FO/NAJ" class='right' ><?php echo number_format($totalnajem,2,',','&nbsp;');  ?></th>
   <td class='right'>Na služby:</td> 
   <th  data-toggle="tooltip" data-placement="top" title="Celkem  zálohy po odečtení FO/NAJ" class='right' ><?php echo number_format($nasluzby,2,',','&nbsp;');  ?></th>
   <td></td>
   <td></td>
   <td></td>
   <td></td>
   
 </tr>




 <tr>
   <th>BALANCE</th>
   <th></th>
   <?php 
   $balance1 = $totalIN - $totalAmount;
   if($balance1 > 0) {  $clr = '#3de51b';  $balance1 = number_format($balance1,2,',','&nbsp;'); }
   if($balance1 < 0) {  $clr = 'red'; $balance1 = number_format($balance1,2,',','&nbsp;'); }
   if($balance1 == 0) { $clr = '#3de51b';  $balance1 = 'OK';  }

 
   ?>



   <th  colspan="2" data-toggle="tooltip" data-placement="top" title="Srovnání všech předpisů a evidovaných plateb"  class='right' style='color: <?php echo $clr; ?>'><?php echo $balance1 ?></th>
   <td class='right'></td> 

   <?php 
   $balance10 =  $nasluzby - $totalOUT;
   if($balance10 > 0) {  $clr2 = '#3de51b';  $balance10 = number_format($balance10,2,',','&nbsp;'); }
   if($balance10 < 0) {  $clr2 = 'red'; $balance10 = number_format($balance10,2,',','&nbsp;'); }
   if($balance10 == 0) {  $balance10 = 'OK';  }

 
   if($totalIN > $totalnajem ) {  $paidFO  = $totalnajem;  }
   elseif($totalIN == $totalnajem)  {  $paidFO  = $totalnajem;  }
   else{  $paidFO = $totalIN; }
    
   ?>



   <th data-toggle="tooltip" data-placement="top" title="Plus(zelená) - byt má přeplatek. Mínus(červená) - byt má nedoplatek" class='right' style='font-size: 18px!important; color: <?php echo $clr2; ?>'  ><?php echo $balance10 ?></th>
   <td></td>
   <td></td>
   <td>(<?php echo $radek; ?> pohybů)</td>
   <td><?php 
    
   
   // $persQuery = "SELECT sum(historyFlatOccupancy) as HISPERSYEAR FROM `fm_occupHistory` WHERE `historyHouseID` = ".$_GET['house']."    ".$persWhereTen.$persWhereOwn.$persWhere1.$persWhere2;

    $persHouseQuery = "SELECT AVG(historyHouseOccupancy) as HISPERSYEARHOUSE, AVG(historyLiftOccupancy) as HISPERSYEARLIFT  FROM `fm_occupHistory` WHERE `historyHouseID` = ".$_GET['house']."    ".$persWhere2;

    $persHousePerson = $GLOBALS["link"]->query($persHouseQuery);
    if ($persHousePerson && mysqli_num_rows($persHousePerson) > 0) {
      while ($rowHousePerson = mysqli_fetch_array($persHousePerson)) {
            $personsHouse = $rowHousePerson['HISPERSYEARHOUSE'] ;
            $personsLift = $rowHousePerson['HISPERSYEARLIFT'] ;

      }
    }

     $persQuery = "SELECT sum(historyFlatOccupancy) as HISPERSYEAR FROM `fm_occupHistory` WHERE `historyHouseID` = ".$_GET['house']."    ".$persWhereTen.$persWhereOwn.$persWhere1.$persWhere2;
    $resultPerson = $GLOBALS["link"]->query($persQuery);
    if ($resultPerson && mysqli_num_rows($resultPerson) > 0) {
      while ($rowPers = mysqli_fetch_array($resultPerson)) {
            $personsMesic = $rowPers['HISPERSYEAR']/12;
            echo "OS/MES ".$personsMesic;
      }
    }
   
   ?></td>
  
 </tr>


</table>
</div>


<?php include('./modules/new.php'); ?>


 
       
<a href="/jobs/wordvyucto.php?load=<?php echo $sanon2loadId; ?>&html=1" id="btnExportW">Uložit jako vyúčtování -  Word </a>

<script type="text/javascript">


$(document).ready( function () {
        $('#tableOUT').DataTable( {
        paging: false ,
 
         "searching": false,
        "columnDefs": [
 
 
 
         ]
        

        } );
      } );

 </script>