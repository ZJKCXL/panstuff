
<h1>Lidé</h1> 

<script>
var toChange = {
  selector: '.tinymce-2change',
  entity_encoding : "raw",
  menubar: false,
  inline: true,
  plugins: "save",
  toolbar: false,
  setup: function (editor) {
        editor.on('change', function () {
           //  editor.save();
           content = editor.getContent();
           //console.log('save >' + editor.id  + '>' + content);
           $.ajax({
             type:       'POST',
             cache:      false,
             url:        '/inside/ajax-save.php?name=' + editor.id,
             data:       'tinydata=' + content,
             success:    function(dt, status, request) {
               // console.log(request.getAllResponseHeaders());
            }
        });
        });
    }
};
tinymce.init(toChange);
</script>


<?php
 /*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
 */
$info_text = "";

$time = time();
$file_name = date("d_m_y", $time) . "TZ" . $time;
$file_name2 = date("d_m_y", $time) . "TZ" . $time;
$table_name = "fm_persons";
$page_name = "osoby";
$deletext = "Položka byla smazána.";
$updatext = "Položka byla opravena.";
$addtext = "Položka byla přidána.";
$jeho = "Položku";

//var_dump($_POST);
?>
 
<a href="index.php?id=<?php echo $page_name; ?>new<?php if ($type) {echo "&amp;type=" . $type;}?>&service1=<?php echo $_REQUEST['service1']; ?>" class="fas fa-plus-circle"><span>Přidat další</span></a>
<a href="index.php?id=<?php echo $page_name; ?>group<?php if ($type) {echo "&amp;type=" . $type;}?>&service1=<?php echo $_REQUEST['service1']; ?>" class="fas fa-plus-circle"><span>Přidat hromadně </span>&nbsp;</a> 

<form action="index.php?id=<?php echo $page_name; ?>&service1=<?php echo $service1; ?>&service2=<?php echo $service2; ?>&service3=<?php echo $service3; ?>&house=<?php echo $_GET['house']; ?>" method="post" name="noname" class='tabupper' style='float: left;'>
 
<select name="service1">
<option value="-1">Dům</option>
<?php
 $Select = "Select personHouse, fm_nickname From fm_persons, fm_house Where fm_house.ID = personHouse  Group by personHouse ";
 $Res = @$GLOBALS["link"]->query($Select);
if ($Res && mysqli_num_rows($Res) > 0) {			
    while ($Row = mysqli_fetch_array($Res)) {	 
    ?>
    <option <?php if($service1 == $Row['personHouse']){ echo " selected "; } ?> value="<?php echo $Row['personHouse']; ?>"><?php echo $Row['fm_nickname']; ?> </option>
    <?php
    }
}
?>
</select>



<input type="submit" name="filtr" class="btn btn-secondary btn-sm soupup" id='filtr' value="Filtrovat"/>

</form>

<p>&nbsp;</p>
<?php


if ($_REQUEST['delete'] > 0) {
    $delquery = 'DELETE FROM  '.$table_name.'  WHERE ID = ' . $_REQUEST[delete] . ' LIMIT 1';
    $delres = $GLOBALS["link"]->query($delquery);
}
$supercount = 0;



if (isset($_POST["group"])) {

    $mydata = explode(PHP_EOL, $_POST['groupdata']);
    foreach ($mydata as &$myrow) {
        //echo $myrow;
        $flatdata = explode(";", $myrow);
        $flatname = trim($flatdata[0]);
        $flatnr = trim($flatdata[1]);
        $flatpersons = trim($flatdata[2]);
        $flatsquare = str_replace (',','.',trim($flatdata[3]));
       // echo $flatname ." | ".$flatnr ." | ".$flatpersons ." | ".$flatsquare ." |  <hr/>"; 

        if(strlen($flatname) > 0) {
        $inPerson = "INSERT INTO `fm_persons` (`personSurname`, `personFlat`, `personHouse`, `personID`, `personFrom` ) VALUES ( '".$flatname."', '".$flatnr."', '".$_POST['myHouses']."', 'Hromadně vloženo', '".$_POST['personFrom']."')";
       
        $inRes = $GLOBALS["link"]->query($inPerson);
            if(mysqli_affected_rows($GLOBALS["link"])) {

                $personID = mysqli_insert_id ( $GLOBALS["link"] );
                if($_POST['myHouses'] > 0) {
                    $inHouse = "INSERT INTO  h".$_POST['myHouses']."flats (`flatPerson`, `flatSquare`, `flatOccupancy`, `flatNr`, `flatVS`) VALUES ( '".$personID."', '".$flatsquare."', '".$flatpersons."', '".$flatnr."', '".$_POST['personFrom'].$flatnr."')";
                    $houseRes = $GLOBALS["link"]->query($inHouse);
                    }

            }
        
            $service1 = $_POST['myHouses'];

        }


        
       
    }

}

if (isset($_REQUEST["send"]) && isset($_REQUEST["newsid"]) && is_numeric($_REQUEST["newsid"]) && $_REQUEST["newsid"] > 0) {
    if (strlen(trim($_GET["newsid"]))) {
        $query = "Update " . $table_name . " set ";
       
        $superquery = "SHOW FULL COLUMNS FROM `".$table_name."`";
        $superes = $GLOBALS["link"]->query($superquery);
        if ($superes && mysqli_num_rows($superes) > 0) {

            while ($supeRow = mysqli_fetch_array($superes)) {

                 if($supeRow[Field] == 'personHouse') { $_POST[$supeRow[Field]] = $_POST['myHouses'] ; }

                if(isset($_POST[$supeRow[Field]])) {
                $col_query  =  $supeRow[Field] ;
                $val_query  =  trim(strip_tags($_POST[$supeRow[Field]])) ;
                if($supercount > 0) { $query .= ", ";  } 
                $supercount++;
                $query .= $col_query . " = '" . $val_query . "'";
                }
   
            }
        }
 
         $query .= " where ID = " . $_REQUEST["newsid"];

        $res = @$GLOBALS["link"]->query($query);
        if ($res && @mysqli_affected_rows($link) > 0) {
            $info_text .= "Položka úspěně uložena.";
            $alertype = " alert-success ";

        } else {
            $info_text .= "Položka nebyla změněna.";
            $alertype = " alert-danger ";
        }

        $goServices = 1; 
        $lastID = $_REQUEST["newsid"]; 

    } else {
        $info_text .= "Chyba během ukládání Položky. Nebyly zadány všechny povinné parametry";
        $alertype = " alert-danger ";
    }

} elseif (isset($_REQUEST["send"]) && !isset($_REQUEST["newsid"])) {



    
   echo  $query = "INSERT INTO ".$table_name." 
    (`personName`, `personSurname`, `personFlat`, `personHouse`, `personID`, `personEmail`, `personPhone`, `personFrom`, `personFamily`, `personNote`) 
    VALUES 
    ( '".$_POST['personName']."', '".$_POST['personSurname']."', '".$_POST['personFlat']."', '".$_POST['myHouses']."', '".$_POST['personID']."', '".$_POST['personEmail']."', '".$_POST['personPhone']."', '".$_POST['personFrom']."', '".$_POST['personFamily']."', '".$_POST['personNote']."' )";

 

    $res = $GLOBALS["link"]->query($query);
    if ($res && mysqli_affected_rows($link) > 0) {
        $info_text .= "Položka úspěně uložena.";
        $alertype = " alert-success ";

        $goServices = 1;
        $lastID = mysqli_insert_id($GLOBALS["link"]); 
 

    } else {
        $info_text .= "Položka nebyla uložena.";
        $alertype = " alert-danger ";
    }

} else {

}

 



if (@$info_text != "") {
    ?>
        <div class="alert <?php echo $alertype; ?>" role="alert"><?php echo $info_text; ?></div>
        <?php
}
?>

<table  id='tableOUT' class="table table-striped table-bordered m-b-0 toggle-circle footable-loaded footable tablet breakpoint demo-inline">
<thead>
<tr>
    <th style='width: 105px'>Edit</th>
    <th>Smazat</th> 
    <th>Jméno</th>
    <th>Dům</th>
    <th>Byt</th>
    <th>Telefon</th>    
    <th>E-mail</th>    
    <th>OD</th>    
    <th>Konec</th>    
</tr>
</thead>
<tbody id='thisTBL' class="row_position" >
<?php
$time = time();
if ($_GET['order'] == 1) {$ordr = 'personSurname';} else { $ordr = $table_name.'.ID  ';}

if($service1 > 0) {
    $where1 = " Where personHouse = ".$service1 ; 
}

$query = "SELECT *,fm_persons.ID as PID FROM $table_name  LEFT JOIN fm_house ON fm_persons.personHouse = fm_house.ID ".$where1." Order by " . $ordr;
$result = $GLOBALS["link"]->query($query);

if ($result && mysqli_num_rows($result) > 0) {
    while ($row = mysqli_fetch_array($result)) {
        ?>
         <tr  id="<?php echo $row['ID']; ?>" >
            <td  class='short center'> 
            <a href='index.php?id=<?php echo $page_name; ?>new&newsid=<?php echo $row['PID']; ?>&service1=<?php echo $_REQUEST['service1']; ?>' class="far fa-edit" ></a>
            </td>
            <td class="short center">
            <a onclick="return confirm('Skutečně chcete položku vymazat z databáze?')" href="index.php?id=<?php echo $page_name; ?>&service1=<?php echo $_REQUEST['service1']; ?>&amp;delete=<?php echo $row['PID']; ?>" class="far fa-times-circle text-danger"></a>
            </td>
            <td class=''><?php echo $row['personSurname']; ?> <?php echo $row['personName']; ?></td>
            <td class='' ><?php echo $row['fm_nickname']; ?></td>
            <td class='' ><?php echo $row['personFlat']; ?></td>
               
            <td class='tinymce-2change' id='personPhone--<?php echo $row['PID']; ?>--<?php echo $table_name; ?>'><?php echo $row['personPhone']; ?> </td> 
            <td class='tinymce-2change' id='personEmail--<?php echo $row['PID']; ?>--<?php echo $table_name; ?>'><?php echo $row['personEmail']; ?> </td> 
            <td><?php echo  getDateFromSQL($row['personFrom']) ; ?></td> 
            <td><?php echo  getDateFromSQL($row['personTo']) ; ?></td>     
        </tr>
         <?php
}
}
?>
 
</tbody>
</table>


<a href="#" id="btnExport"> EXCEL </a>

<script type="text/javascript">

$(document).ready( function () {
        $('#tableOUT').DataTable( {
        paging: false ,
        "order": [[ 2,  "desc" ]], 
        "searching": true,
        "columnDefs": [
 
         { "orderable": false, "targets": 0 },
         { "orderable": false, "targets": 1 },
         { "orderable": false, "targets": 5 },

         { "orderable": false, "targets": 6 }
 
         ]
        

        } );
      } );

    $("#btnExport").click(function (e) {
        var htmltable= document.getElementById('tableOUT');
        var html = htmltable.outerHTML;
        while (html.indexOf('á') != -1) html = html.replace('á', '&aacute;');
        while (html.indexOf('Á') != -1) html = html.replace('Á', '&Aacute;');
        while (html.indexOf('č') != -1) html = html.replace('č', '&#269;');
        while (html.indexOf('Č') != -1) html = html.replace('Č', '&#268;');
        while (html.indexOf('é') != -1) html = html.replace('é', '&eacute;');
        while (html.indexOf('É') != -1) html = html.replace('É', '&Eacute;');
        while (html.indexOf('í') != -1) html = html.replace('í', '&iacute;');
        while (html.indexOf('Í') != -1) html = html.replace('Í', '&Iacute;');
        while (html.indexOf('ó') != -1) html = html.replace('ó', '&oacute;');
        while (html.indexOf('Ó') != -1) html = html.replace('Ó', '&Oacute;');
        while (html.indexOf('ú') != -1) html = html.replace('ú', '&uacute;');
        while (html.indexOf('Ú') != -1) html = html.replace('Ú', '&Uacute;');
        while (html.indexOf('º') != -1) html = html.replace('º', '&ordm;');
        while (html.indexOf('ň') != -1) html = html.replace('ň', '&#328;');
        while (html.indexOf('Ň') != -1) html = html.replace('Ň', '&#327;');
        while (html.indexOf('š') != -1) html = html.replace('š', '&#353;');
        while (html.indexOf('Š') != -1) html = html.replace('Š', '&#352;');
        while (html.indexOf('ř') != -1) html = html.replace('ř', '&#345;');
        while (html.indexOf('Ř') != -1) html = html.replace('Ř', '&#344;');
        while (html.indexOf('ť') != -1) html = html.replace('ť', '&#357;');
        while (html.indexOf('Ť') != -1) html = html.replace('Ť', '&#356;');
        while (html.indexOf('´') != -1) html = html.replace('´', '&#39;');
        while (html.indexOf('ě') != -1) html = html.replace('ě', '&#283;');
        while (html.indexOf('Ě') != -1) html = html.replace('Ě', '&#282;');
        while (html.indexOf('Ý') != -1) html = html.replace('Y', '&Yacute;');
        while (html.indexOf('ý') != -1) html = html.replace('ý', '&yacute;');
        while (html.indexOf('ž') != -1) html = html.replace('ž', '&#382;');
        while (html.indexOf('Ž') != -1) html = html.replace('Ž', '&#381;');
        var result = 'data:application/vnd.ms-excel,' + encodeURIComponent(html);
        this.href = result;
        this.download = "SLUZBY.xls";
        return true;
    });

    $( ".row_position" ).sortable({
        delay: 150,
        stop: function() {
            var selectedData = new Array();
            $('.row_position>tr').each(function() {
                selectedData.push($(this).attr("id"));
            });
            updateOrder(selectedData);
          }
    });
    $( ".xx" ).mouseenter(function() {
        $(".row_position").sortable("enable");
    }).mouseleave(function() {
        $(".row_position").sortable("disable");
    });

    $( ".tinymce-2change" ).mouseenter(function() {
        $(".row_position").sortable("disable");
    });

    function updateOrder(data) {
        console.log(data);
        $.ajax({
            url:'/inside/ajax-order.php?name=' + '<?php echo "serviceOrder--b--" . $table_name; ?>',
            type:'post',
            data:{position:data},
            success:function(){
                console.log(data);
            }
        })
    }


</script>