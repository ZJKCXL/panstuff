<style>
        .bignebig{
         display: block;
         float: left;
         margin-top: 0px;
         width: auto;
        }
        .bigneg{
         width: 200px;
         margin-top: 4px;
 
        }  
        .lft {
            float: left;
            margin-right: 15px;
            display: inline ;
        }
  .deletecomp {
    display: block;
    float: right;
    background: red;
    color: #fff;
    padding: 5px 15px;
   }  
   .deletecomp:hover{
       color: #fff;
      background: #b53b3b
   }
#exampleFormControlSelect2_chosen {
    width: calc(100% - 200px)!important ;
}
  

</style>

<script>

$('.chosen-select').chosen();
$.validator.setDefaults({ ignore: ":hidden:not(select)" });

// validation of chosen on change
if ($("select.chosen-select").length > 0) {
    $("select.chosen-select").each(function() {
        if ($(this).attr('required') !== undefined) {
            $(this).on("change", function() {
                $(this).valid();
            });
        }
    });
}

$('#kontakt').validate({
    errorPlacement: function (error, element) {
        console.log("placement");
        if (element.is("select.chosen-select")) {
            console.log("placement for chosen");
            // placement for chosen
            element.next("div.chzn-container").append(error);
        } else {
            // standard placement
            error.insertAfter(element);
        }
    }
});
</script>

<?php
 
$page_name = "counters";

if (isset($_REQUEST["newsid"]) && $_REQUEST["newsid"]>0)
{
    $newsid = $_REQUEST["newsid"];
}
else
{
    $newsid = false;
}


if ($newsid)
{
    $query = "SELECT * FROM ".$table_name." WHERE ID =".$newsid;
   
    $res = $GLOBALS["link"]->query($query);
    if ($res && @mysqli_num_rows($res)>0)
    {
        $row = mysqli_fetch_array($res);
    }
}



?>

<h1>Hromadné údajů z měřidel a podobných zdrojů</h1>
 
<form ENCTYPE="multipart/form-data" action="index.php?id=<?php echo $page_name; ?><?php if($house>0){echo "&house=".$house;}?><?php if($newsid>0){echo "&newsid=",$newsid;}?>" method="post" name="noname" id="kontakt">
 
<div class="row">
 
 <fieldset  > 
 
<?php
echo bootInput('Název měřidla:','required','text',$row['waterName'],'waterName',$js); 
echo bootInput('Odečteno:','required','date',$row['waterDate'],'waterDate',$js); 
echo bootInput('Poznámka:' ,'','text',$row['waterNote'],'waterNote',$js); 
 
 ?> 

<div class="input-group mb-3">
			<div class="input-group-prepend">
			<span class="input-group-text" id="inputGroup-sizing-default">Typ odečtu:</span>
			</div>
			<select  class="form-control  chosen-select  " required id="waterSource" name="waterSource" >
            <option value='' >vyber</option>
            <option value='1'>Dálkový odečet</option>
            <option value='2'>Osobní odečet</option>
    		</select>
</div>


<div  >
<div class="input-group mb-3">
<div class="input-group-prepend">
<select class="custom-select" id="inputGroupSelect01" name='waterFileType'>
    <option value=''>Zdroj souboru...</option>
    <option value="1">ERAM EXPORT</option>
    <option value="2">EWM Export (OLD HRA)</option>
    <option value="3">Osobní odečty (todo)</option>
  </select>
  </div>

  <div class="custom-file">
    <input type="file" name='waterFile' class="custom-file-input" id="inputGroupFile02">
    <label class="custom-file-label" style='left: 0px;' for="inputGroupFile02">Najít soubor</label>
  </div>
 
</div>
</div>


</fieldset>


</div>



<input type="hidden"  name="counterTimeID" value="<?php echo time(); ?>" />
<input type="submit" value="Vložit a zpracovat " name="group" class="btn btn-primary  " /> 
                                      
</form>
