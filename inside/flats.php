
<h1>Byty v domě <?php echo getHouseNickByID($_GET['house']); ?></h1> 
<?php include('./modules/warning.php'); ?>
<script>
var toChange = {
  selector: '.tinymce-2change',
  entity_encoding : "raw",
  menubar: false,
  inline: true,
  plugins: "save",
  toolbar: false,
  setup: function (editor) {
        editor.on('change', function () {
           //  editor.save();
           content = editor.getContent();
            console.log('save >' + editor.id  + '>' + content);
           $.ajax({
             type:       'POST',
             cache:      false,
             url:        '/inside/ajax-save.php?name=' + editor.id,
             data:       'tinydata=' + content,
             success:    function(dt, status, request) {
               // console.log(request.getAllResponseHeaders());
            }
        });
        });
    }
};
tinymce.init(toChange);
</script>


<?php
 /*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
 */
$info_text = "";

$time = time();
$file_name = date("d_m_y", $time) . "TZ" . $time;
$file_name2 = date("d_m_y", $time) . "TZ" . $time;
$table_name = "h".$_GET['house']."flats";
$page_name = "flats";
$deletext = "Položka byla smazána.";
$updatext = "Položka byla opravena.";
$addtext = "Položka byla přidána.";
$jeho = "Položku";
$house = $_GET['house'];
?>
 
<a href="index.php?id=<?php echo $page_name; ?>new<?php if ($_GET['house']) {echo "&amp;house=" . $_GET['house'];}?>" class="add fas fa-plus-circle"><span>Přidat další</span></a>
 
<?php

 
if ($_REQUEST['delete'] > 0) {
 echo   $delquery = 'DElete From '.$table_name.'  WHERE  `ID` = ' . $_REQUEST[delete] . '  ';
    $delres = $GLOBALS["link"]->query($delquery);
}
$supercount = 0;

if (isset($_REQUEST["send"]) && isset($_REQUEST["newsid"]) && is_numeric($_REQUEST["newsid"]) && $_REQUEST["newsid"] > 0) {
    if (strlen(trim($_GET["newsid"]))) {
        $query = "Update " . $table_name . " set ";
       
        $superquery = "SHOW FULL COLUMNS FROM `".$table_name."`";
        $superes = $GLOBALS["link"]->query($superquery);
        if ($superes && mysqli_num_rows($superes) > 0) {

            while ($supeRow = mysqli_fetch_array($superes)) {
                if(isset($_POST[$supeRow[Field]])) {
                $col_query  =  $supeRow[Field] ;
                $val_query  =  trim(strip_tags($_POST[$supeRow[Field]])) ;
                if($supercount > 0) { $query .= ", ";  } 
                $supercount++;
                $query .= $col_query . " = '" . $val_query . "'";
                }
   
            }
        }
 
        $query .= " where ID = " . $_REQUEST["newsid"];

        $res = @$GLOBALS["link"]->query($query);
        if ($res && @mysqli_affected_rows($link) > 0) {
            $info_text .= "Položka úspěně uložena.";
            $alertype = " alert-success ";

        } else {
            $info_text .= "Položka nebyla změněna.";
            $alertype = " alert-danger ";
        }

        $goServices = 1; 
        $lastID = $_REQUEST["newsid"]; 

    } else {
        $info_text .= "Chyba během ukládání Položky. Nebyly zadány všechny povinné parametry";
        $alertype = " alert-danger ";
    }

} elseif (isset($_REQUEST["send"]) && !isset($_REQUEST["newsid"])) {


    $_POST['flatSquare']  = str_replace( ',','.', $_POST['flatSquare']);
    $_POST['flatDeal'] = str_replace( ',','.', $_POST['flatDeal']);
    
  $query = "INSERT INTO ".$table_name." 
    (`flatOwner`, `flatPerson`, `flatSquare`, `flatDeal`, `flatNr`,   `flatVS`, `flatLift` ) 
    VALUES 
    ( '".$_POST['flatOwner']."', '".$_POST['flatPerson']."', '".$_POST['flatSquare']."', '".$_POST['flatDeal']."', '".$_POST['flatNr']."', '".$_POST['flatVS']."', '".$_POST['flatLift']."')";

    $res = $GLOBALS["link"]->query($query);
    if ($res && mysqli_affected_rows($link) > 0) {
        $info_text .= "Položka úspěně uložena.";
        $alertype = " alert-success ";

        $goServices = 1;
        $lastID = mysqli_insert_id($GLOBALS["link"]); 
 

    } else {
        $info_text .= "Položka nebyla uložena.";
        $alertype = " alert-danger ";
    }

} else {

}

 

if (@$info_text != "") {
    ?>
        <div class="alert <?php echo $alertype; ?>" role="alert"><?php echo $info_text; ?></div>
        <?php
}
?>
<div class="table-responsive">
<table  id='tableOUT' class="table table-striped table-bordered m-b-0 toggle-circle footable-loaded footable tablet breakpoint demo-inline">
<thead>
<tr>
    <th style='width: 105px'>Edit</th>
    <th>Smazat</th> 
    <th>Č.</th>
    <th>Pořadí</th>
    <th>Vlastník</th>
    <th>Nájemník</th>
    <th>TF</th>
    <th>@</th>
    <th>Výtah</th>
    <th>Osob</th>
    <th>M<sup>2</sup></th>    
    <th>Podíl v %</th>    
    <th>Var. symbol</th>  
 
</tr>
</thead>
<tbody id='thisTBL' class="row_positionx" >
<?php
$time = time();
$query = "SELECT * FROM ".$table_name." Order by flatOrder, ABS(flatNr) " ;
$result = $GLOBALS["link"]->query($query);

$flatSquare = $totalDeal = 0;

if ($result && mysqli_num_rows($result) > 0) {
    while ($row = mysqli_fetch_array($result)) {
        ?>
         <tr  id="<?php echo $row['ID']; ?>" >
            <td  class='short center'><a href='index.php?id=<?php echo $page_name; ?>new&newsid=<?php echo $row['ID']; ?>&house=<?php echo $_GET['house']; ?>' class="far fa-edit" ></a>
            </td>
            <td class="short center"><a onclick="return confirm('Skutečně chcete položku vymazat z databáze?')" href="index.php?id=<?php echo $page_name; ?>&house=<?php echo $_GET['house']; ?>&amp;delete=<?php echo $row['ID']; ?>" class="far fa-times-circle text-danger"></a></td>
            <td class='short center tinymce-2change' id='flatNr--<?php echo $row['ID']; ?>--<?php echo $table_name; ?>'><?php echo $row['flatNr']; ?>  </td>
            <td class='short center'  title='Přetáhněte položku myší'> <i class="xx fas fa-sort"></i> </td>
            <?php $flatrow = $flatrow + 1; ?>
  
            <td class='' ><?php 
            
           $testDate = date(Y)."-".date(m)."-01";
            echo getPersonByID(getActualOwner($testDate,$row['ID'],$house) ); 
            
            ?></td>
            <td class='' ><?php echo getPersonByID(getActualTenant($testDate,$row['ID'],$house) );  ?></td>  
            <td class='short center phonecont' > <?php echo contactExist('tf',getActualOwner($testDate,$row['ID'],$house) );  ?> <?php if( getActualOwner($testDate,$row['ID'],$house) != getActualTenant($testDate,$row['ID'],$house) ) { echo contactExist('tf',getActualTenant($testDate,$row['ID'],$house) ); } ?></td>  
            <td class='short center mailcont' > <?php echo contactExist('mail',getActualOwner($testDate,$row['ID'],$house) );  ?> <?php if( getActualOwner($testDate,$row['ID'],$house) != getActualTenant($testDate,$row['ID'],$house) ) { echo contactExist('mail',getActualTenant($testDate,$row['ID'],$house) ); } ?></td>  
            
            <?php

 
                $emaily .=  contactPlainEmail(getActualOwner($testDate,$row['ID'],$house));
                $emaily .=  "; ";
                $emaily .=  contactPlainEmail(getActualTenant($testDate,$row['ID'],$house));
                $emaily .=  "; ";

            ?>

            <?php  if($row['flatLift'] == 1) { $lift = $row['flatOccupancy'] ; } else { $lift = '';   }   ?>
            <?php $liftOccupancy = $liftOccupancy + $lift; ?>
            <td  class='short center'  ><?php echo $lift; ?></td>      
            <?php if(getPersonsInFlat($testDate,$house,$row['ID']) != $row['flatOccupancy'] ) { 
                    $wrongclass = 'WRN'; 
                    $wrongTitle = 'data-toggle="tooltip" data-placement="top" title="Počet osob nesouhlasí s evidenčními listy" ';
                } else {  
                    $wrongclass = '';  
                    $wrongTitle = '';
                    } 
            ?>     
            <td  <?php echo $wrongTitle; ?> class='<?php echo $wrongclass; ?> short center tinymce-2change' id='flatOccupancy--<?php echo $row['ID']; ?>--<?php echo $table_name; ?>'>

            <?php echo   $row['flatOccupancy'] ; ?>
            </td>   
            <?php $flatOccupancy = $flatOccupancy + $row['flatOccupancy']; ?>
            <td class='short center tinymce-2change' id='flatSquare--<?php echo $row['ID']; ?>--<?php echo $table_name; ?>'><?php echo $row['flatSquare']; ?> </td> 
            <?php $flatSquare = $flatSquare + $row['flatSquare']; ?>
            <td class='short center tinymce-2change' id='flatDeal--<?php echo $row['ID']; ?>--<?php echo $table_name; ?>'><?php echo  $row['flatDeal']  ; ?></td> 
            <?php $totalDeal = $totalDeal + $row['flatDeal']; ?>
            <td class=' tinymce-2change' id='flatVS--<?php echo $row['ID']; ?>--<?php echo $table_name; ?>'><?php echo  $row['flatVS']  ; ?></td> 
            
        </tr>
         <?php
}
}
?>
 
 </tbody>
<tr>
    <th>CELKEM</th>
    <th> </th> 
    <th  class='center'><?php echo $flatrow; ?> </th>    
    <th> </th>
    <th> </th><th> </th><th> </th>
    
    <th><a class='color' href='<?php  echo 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>'><i class="fas fa-sync"></i> Reload</a></th>  
    <th class='center'><?php echo $liftOccupancy; ?></th>
    <th class='center'><?php echo $flatOccupancy; ?></th>
    <th><?php echo $flatSquare; ?>m<sup>2</sup></th>    
    <th><?php echo $totalDeal; ?>%</th>    
    <th> </th>

</tr>

</table>
</div>
<a href="mailto:panstav@panstav.cz?bcc=<?php echo $emaily; ?>" id="btnExportOUTLOOK"> EMAILY </a>
<a href="#" id="btnExport"> EXCEL </a>

<script type="text/javascript">

$(document).ready( function () {
        $('#tableOUT').DataTable( {
        paging: false ,
       "searching": true,
        "columnDefs": [
 
         { "orderable": false, "targets": 0 },
         { "orderable": false, "targets": 1 },
         { "orderable": false, "targets": 0 },
         { "orderable": false, "targets": 3 },
         { "orderable": false, "targets": 6 },
         { "orderable": false, "targets": 7 },
         { "orderable": false, "targets": 8 },
         { "orderable": false, "targets": 9 }
 
         ]
        

        } );
      } );

    $("#btnExport").click(function (e) {

      

        var htmltable= document.getElementById('tableOUT');
        var html = htmltable.outerHTML;
        while (html.indexOf('á') != -1) html = html.replace('á', '&aacute;');
        while (html.indexOf('Á') != -1) html = html.replace('Á', '&Aacute;');
        while (html.indexOf('č') != -1) html = html.replace('č', '&#269;');
        while (html.indexOf('Č') != -1) html = html.replace('Č', '&#268;');
        while (html.indexOf('é') != -1) html = html.replace('é', '&eacute;');
        while (html.indexOf('É') != -1) html = html.replace('É', '&Eacute;');
        while (html.indexOf('í') != -1) html = html.replace('í', '&iacute;');
        while (html.indexOf('Í') != -1) html = html.replace('Í', '&Iacute;');
        while (html.indexOf('ó') != -1) html = html.replace('ó', '&oacute;');
        while (html.indexOf('Ó') != -1) html = html.replace('Ó', '&Oacute;');
        while (html.indexOf('ú') != -1) html = html.replace('ú', '&uacute;');
        while (html.indexOf('Ú') != -1) html = html.replace('Ú', '&Uacute;');
        while (html.indexOf('º') != -1) html = html.replace('º', '&ordm;');
        while (html.indexOf('ň') != -1) html = html.replace('ň', '&#328;');
        while (html.indexOf('Ň') != -1) html = html.replace('Ň', '&#327;');
        while (html.indexOf('š') != -1) html = html.replace('š', '&#353;');
        while (html.indexOf('Š') != -1) html = html.replace('Š', '&#352;');
        while (html.indexOf('ř') != -1) html = html.replace('ř', '&#345;');
        while (html.indexOf('Ř') != -1) html = html.replace('Ř', '&#344;');
        while (html.indexOf('ť') != -1) html = html.replace('ť', '&#357;');
        while (html.indexOf('Ť') != -1) html = html.replace('Ť', '&#356;');
        while (html.indexOf('´') != -1) html = html.replace('´', '&#39;');
        while (html.indexOf('ě') != -1) html = html.replace('ě', '&#283;');
        while (html.indexOf('Ě') != -1) html = html.replace('Ě', '&#282;');
        while (html.indexOf('Ý') != -1) html = html.replace('Y', '&Yacute;');
        while (html.indexOf('ý') != -1) html = html.replace('ý', '&yacute;');
        while (html.indexOf('ž') != -1) html = html.replace('ž', '&#382;');
        while (html.indexOf('Ž') != -1) html = html.replace('Ž', '&#381;');
        var result = 'data:application/vnd.ms-excel,' + encodeURIComponent(html);
        this.href = result;
        this.download = "SLUZBY.xls";
        return true;
    });

    $( ".row_position" ).sortable({
        delay: 150,
        stop: function() {
            var selectedData = new Array();
            $('.row_position>tr').each(function() {
                selectedData.push($(this).attr("id"));
            });
            updateOrder(selectedData);
          }
    });
    $( ".xx" ).mouseenter(function() {
        $(".row_position").sortable("enable");
    }).mouseleave(function() {
        $(".row_position").sortable("disable");
    });

    $( ".tinymce-2change" ).mouseenter(function() {
        $(".row_position").sortable("disable");
    });

    function updateOrder(data) {
        console.log(data);
        $.ajax({
            url:'/inside/ajax-order.php?name=' + '<?php echo "flatOrder--b--" . $table_name; ?>',
            type:'post',
            data:{position:data},
            success:function(){
                console.log(data);
            }
        })
    }


</script>