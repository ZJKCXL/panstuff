
<h1>Kalendář</h1> 

<script>
var toChange = {
  selector: '.tinymce-2change',
  entity_encoding : "raw",
  menubar: false,
  inline: true,
  plugins: "save",
  toolbar: false,
  setup: function (editor) {
        editor.on('change', function () {
           //  editor.save();
           content = editor.getContent();
           //console.log('save >' + editor.id  + '>' + content);
           $.ajax({
             type:       'POST',
             cache:      false,
             url:        '/inside/ajax-save.php?name=' + editor.id,
             data:       'tinydata=' + content,
             success:    function(dt, status, request) {
               // console.log(request.getAllResponseHeaders());
            }
        });
        });
    }
};
tinymce.init(toChange);
</script>


<?php
 /*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
 */
$info_text = "";

$time = time();
$file_name = date("d_m_y", $time) . "TZ" . $time;
$file_name2 = date("d_m_y", $time) . "TZ" . $time;
$table_name = "fm_calender";
$page_name = "calendar";
$deletext = "Položka byla smazána.";
$updatext = "Položka byla opravena.";
$addtext = "Položka byla přidána.";
$jeho = "Položku";
 //var_dump($_POST);
?>
<a href="index.php?id=<?php echo $page_name; ?>new<?php if ($_GET['house']) {echo "&amp;house=" . $_GET['house'];}?>" class="add fas fa-plus-circle"><span>Přidat další</span></a>

<p>&nbsp;</p>
<?php
if ($_REQUEST['delete'] > 0) {
      $delquery = 'Delete From `'.$table_name.'`   WHERE `'.$table_name.'`.`ID` = ' . $_REQUEST[delete] . ' LIMIT 1';
    $delres = $GLOBALS["link"]->query($delquery);
}
$supercount = 0;
 
if (isset($_REQUEST["send"]) && isset($_REQUEST["newsid"]) && is_numeric($_REQUEST["newsid"]) && $_REQUEST["newsid"] > 0) {
    if (strlen(trim($_GET["newsid"]))) {
        $query = "Update " . $table_name . " set ";
       
        $superquery = "SHOW FULL COLUMNS FROM `".$table_name."`";
        $superes = $GLOBALS["link"]->query($superquery);
        if ($superes && mysqli_num_rows($superes) > 0) {

            while ($supeRow = mysqli_fetch_array($superes)) {
                if(isset($_POST[$supeRow[Field]])) {
                $col_query  =  $supeRow[Field] ;
                $val_query  =  trim(strip_tags($_POST[$supeRow[Field]])) ;
                if($supercount > 0) { $query .= ", ";  } 
                $supercount++;
                $query .= $col_query . " = '" . $val_query . "'";
                }
   
            }
        }
 
        $query .= " where ID = " . $_REQUEST["newsid"];

        $res = @$GLOBALS["link"]->query($query);
        if ($res && @mysqli_affected_rows($link) > 0) {
            $info_text .= "Položka úspěně uložena.";
            $alertype = " alert-success ";

        } else {
            $info_text .= "Položka nebyla změněna.";
            $alertype = " alert-danger ";
        }

        $goServices = 1; 
        $lastID = $_REQUEST["newsid"]; 

    } else {
        $info_text .= "Chyba během ukládání Položky. Nebyly zadány všechny povinné parametry";
        $alertype = " alert-danger ";
    }

} elseif (isset($_REQUEST["send"]) && !isset($_REQUEST["newsid"])) {

    $query = "INSERT INTO `".$table_name."` (  `calName`, `calDate`, `calTxt`, `calHouse`) VALUES ( '".$_POST['calName']."', '".$_POST['calDate']."', '".$_POST['calTxt']."', '".$_POST['calHouse']."')";

    $res = $GLOBALS["link"]->query($query);
    if ($res && mysqli_affected_rows($link) > 0) {
        $info_text .= "Položka úspěně uložena.";
        $alertype = " alert-success ";

        $goServices = 1;
        $lastID = mysqli_insert_id($GLOBALS["link"]); 
 

    } else {
        $info_text .= "Položka nebyla uložena.";
        $alertype = " alert-danger ";
    }

} else {

}

 

if (@$info_text != "") {
    ?>
        <div class="alert <?php echo $alertype; ?>" role="alert"><?php echo $info_text; ?></div>
        <?php
}
?>

<div class="row">

<div class="table-responsive">
<table  id='tableOUT' class="table table-striped table-bordered m-b-0 toggle-circle footable-loaded footable tablet breakpoint demo-inline">

<tr>
    <th style='width: 105px'>Edit</th>
    <th>Smazat</th> 
    <th>Dům</th>
    <th>Termin</th>
    <th>Event</th>
    <th>Text</th>
 
</tr>
<tbody id='thisTBL' class="row_position" >
<?php
$time = time();
if ($_GET['order'] == 1) {$ordr = 'serviceName';} else { $ordr = 'ID  ';}

  $query = "SELECT *,".$table_name.".ID as CID FROM ".$table_name." left join fm_house ON fm_house.ID = calHouse ORDER BY calDate ";
$result = $GLOBALS["link"]->query($query);

if ($result && mysqli_num_rows($result) > 0) {
    while ($row = mysqli_fetch_array($result)) {
        ?>
         <tr  id="<?php echo $row['ID']; ?>" >
            <td  class='short center'> 
            <a href='index.php?id=<?php echo $page_name; ?>new&newsid=<?php echo $row['CID']; ?>' class="far fa-edit" ></a>
            </td>
            <td class="short center">
            <a onclick="return confirm('Skutečně chcete položku vymazat z databáze?')" href="index.php?id=<?php echo $page_name; ?>&amp;delete=<?php echo $row['CID']; ?>" class="far fa-times-circle text-danger"></a>
            </td>
            <td><?php echo $row['fm_nickname']; ?></td>
            <td class='tinymce-2change' id='calName--<?php echo $row['CID']; ?>--<?php echo $table_name; ?>'><?php echo $row['calName']; ?></td>
            <td><?php echo getDateFromSQL($row['calDate']); ?></td>
            <td class='tinymce-2change' id='calTxt--<?php echo $row['CID']; ?>--<?php echo $table_name; ?>'><?php echo $row['calTxt']; ?></td>                       

        </tr>
         <?php
}
}
?>
 
</tbody>
</table>
</div>
 
</div>