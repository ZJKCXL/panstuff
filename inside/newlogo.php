 
<style>

#newlogopic {
    float: left;
}

#newlogopic .fas {
    font-size: 30px;
    background: #9ab420;
    color: #fff;
    padding: 10px;
    margin: 0;
    border: 0;
    float: left;
    border: 1px solid #fff;
    border-width: 0 1px 1px 0;
    }
#newlogopic .fas:nth-child(2) {
    background: #f45220;
}
#newlogopic .fas:nth-child(3) {
    background: #029bb8;
    clear: left;
}
#newlogopic .fas:nth-child(4) {
    background: #b90109;
}
#name {
    font-family: 'Oswald', sans-serif;
    font-size: 34px;
    display:  block;
    float: left;
}
#name span {
    display:  block;
    float: left;
    padding-left: 15px;
    line-height: 1.45em;
    margin-top: -7px;
}
#name span:nth-child(2) {
   clear: left;
   font-size: 30px;
}
#name span:nth-child(3) {
   clear: left;
   font-size: 24px;
   color: #c0c0c0
}
</style>

<div id='newlogo'>
<div id='newlogopic'>
<i class="fas fa-key"></i><i class="fas fa-wrench"></i><i class="fas fa-coins"></i><i class="fas fa-broom"></i>
</div>
<div id='name'>
<span>PANSTAV</span>
<span>SLUŽBY</span>
<span>s.r.o.</span>
</div>
</div>